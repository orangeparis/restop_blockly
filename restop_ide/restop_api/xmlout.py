"""

    produce a output.xml file for robotframework from output.yaml


"""
import time
from datetime import datetime
import codecs
from xml.sax.saxutils import escape
import yaml
from jinja2 import Template
from robot.rebot import rebot


date_format_sample='20170130 18:15:50.611'


# def xmlescape(a):
#     return escape( str(a) )
#
# class FilterModule(object):
#      ''' xmlescape filter '''
#      def filters(self):
#          return {
#
#              # filter map
#              'xmlescape': xmlescape
#
#          }


def convert_timestamp(timestamp):
    """

    # YYYYMMDD hh:mm:ss.mil
    :param timestamp:
    :return:
    """
    t=  datetime.fromtimestamp(timestamp).isoformat()
    t= datetime.fromtimestamp(timestamp).strftime('%Y%m%d %H:%M:%S')
    t= t + '.000'
    return t


# xml_header='''\
# <?xml version="1.0" encoding="UTF-8"?>
# <robot generated="20170130 18:15:50.611" generator="Restop-Blocly">
#
# <suite
#  source="./test_livebox.robot"
#  id="s1" name="Test Livebox">
#
#   {% block tests %}
#   {% endblock %}
#
# <status status="PASS" endtime="20170130 18:17:36.569" starttime="20170130 18:15:50.614"></status>
# </suite>
#
# <statistics>
#     <total>
#         <stat fail="0" pass="1">Critical Tests</stat>
#         <stat fail="0" pass="1">All Tests</stat>
#     </total>
#     <tag>
#     </tag>
#     <suite>
#         <stat fail="0" id="s1" name="Test Livebox" pass="1">Test Livebox</stat>
#     </suite>
# </statistics>
# <errors>
# </errors>
#
# </robot>
# '''
#
#
# xml_test='''\
# <test id="s1-t1" name="demo">
#
#   {% block keywords %}
#   {% endblock %}
#
# <status status="PASS" endtime="20170130 18:17:36.568" critical="yes" starttime="20170130 18:15:50.720"></status>
# </test>
# '''
#
# #
# #  keyword
# #
#
# keyword_message= '''<msg timestamp="{{timestamp}}" level="{{ level }}">{{ message }}</msg>'''
#
#
# xml_keyword_defaults= dict(
#     keyword_name= "Keyword Name",
#     library= "restop_client",
#     doc= "documentation for keyword",
#     arguments= ["v1","v2"],
#     msgs= [
#         dict(timestamp="20170130 18:15:50.772",level="INFO",message="message one"),
#         dict(timestamp="20170130 18:15:50.772",level="DEBUG",message="message two"),
#     ],
#
#     status= "PASS",
#     starttime= "20170130 18:15:51.449",
#     endtime= "20170130 18:15:51.449",
#
# )
# xml_keyword='''\
# <kw name="{{ keyword_name }}" library="restop_client">
#     <doc>{{doc}}</doc>
#     <arguments>
#         {% for argument in arguments -%}
#         <arg>{{ argument }}</arg>
#         {%- endfor %}
#     </arguments>
#     {% for msg in msgs -%}
#     <msg timestamp="{{ msg.timestamp }}" level="{{ msg.level }}">{{ msg.message }}</msg>
#     {% endfor -%}
#     <status status="{{ status }}" endtime="{{ endtime }}" starttime="{{ starttime }}"></status>
#
#   {% block keyword %}
#   {% endblock %}
#
# </kw>
# '''
#
#
# # the outer part of a keyword
# xml_keyword_outer="""\
# <kw name="{{ keyword_name }}" library="restop_client">
# {{ content }}
# </kw>
# """
#
#
# # the inner part of a keyword
# xml_keyword_inner='''\
#     <doc>{{doc}}</doc>
#     <arguments>
#         {% for argument in arguments -%}
#         <arg>{{ argument }}</arg>
#         {%- endfor %}
#     </arguments>
#     {% for msg in msgs -%}
#     <msg timestamp="{{ msg.timestamp }}" level="{{ msg.level }}">{{ msg.message }}</msg>
#     {% endfor -%}
#     <status status="{{ status }}" endtime="{{ endtime }}" starttime="{{ starttime }}"></status>
# '''

#
# macro_keyword_inside='''\
# {% macro fill_keyword( doc, arguments, msgs ,status, startime, endtime ) -%}
#     <doc>{{ doc }}</doc>
#     <arguments>
#         {% for argument in arguments -%}
#         <arg>{{ argument }}</arg>
#         {% endfor %}
#     </arguments>
#     {%for msg in msgs -%}
#     <msg timestamp="{{ msg.timestamp }}" level="{{ msg.level }}">{{ msg.message }}</msg>
#     {%- endfor%}
#     <status status="{{ status }}" endtime="{{ endtime }}" starttime="{{ starttime }}"></status>
# {%- endmacro %}
#
# {% call fill_keyword( doc,arguments,msgs,status,starttime,endtime ) %}
# {% endcall %}
# '''

xml_suite_header='''\
<?xml version="1.0" encoding="UTF-8"?>
<robot generated="{{ started }}" generator="{{ generator }}">

<suite source="{{ source }}" id="{{ id }}" name="{{ name }}">
'''

xml_suite_footer='''\
<status status="{{ status }}" endtime="{{ ended }}" starttime="{{ started }}"></status>
</suite>
</robot>
'''

rest='''
<statistics>
    <total>
        <stat fail="0" pass="1">Critical Tests</stat>
        <stat fail="0" pass="1">All Tests</stat>
    </total>
    <tag>
    </tag>
    <suite>
        <stat fail="0" id="{{ id }}" name="{{ name }}" pass="1">Test Livebox</stat>
    </suite>
</statistics>
<errors>
</errors>
'''


xml_test_header='''\
<test id="{{ id }}" name="{{ name }}">
'''

# critical="yes"
xml_test_footer='''\
<status status="{{ status }}" endtime="{{ ended }}" critical="{{ critical }}" starttime="{{ started }}"></status>
</test>
'''


xml_keyword_header='''\
<kw name="{{ keyword }}" library="{{ library }}">
    <doc>{{doc}}</doc>
    <arguments>
        {% for argument in arguments -%}
        <arg>{{ argument }}</arg>
        {%- endfor %}
    </arguments>
    {% for msg in msgs -%}
    <msg timestamp="{{ msg.timestamp }}" level="{{ msg.level }}">{{ msg.content | escape }}</msg>
    {% endfor -%}
    <status status="{{ status.status }}" endtime="{{ status.endtime }}" starttime="{{ status.starttime }}"></status>
'''

xml_keyword_footer='''\
</kw>
'''


class XmlOut(object):
    """

        generate a robotframework output.xml from a trace.yml ( produced by restop_blockly )



    """

    def __init__(self,data,debug=False):
        """

        :param data: list ( from yaml trace )
        """
        self.data= data
        self._xml=[]
        self._html=''

        self.debug= debug

        self._last_suite_data= None
        self._last_test_data= None
        self._last_keyword_data= None

        self.count_keywords = 0
        self.count_test= 0


    @classmethod
    def from_string(cls,lines):
        """

        :param lines: string file content
        :return:
        """
        data= yaml.load(lines)
        obj= cls(data)
        return obj


    @classmethod
    def from_file(cls,filename):
        """

        :param filename:
        :return:
        """
        data= yaml.load(file(filename))
        return cls(data)


    def scan_children_for_error(self,element,state=None):
        """
            traverse yaml tree to see if one child is on error

        :return:
        """
        state= state or 'PASS'
        if 'body' in element:
            # it is a block (suite/tesst/composite keyword)
            children= element['body']
            #state['level']= state['level'] + 1
            if children:
                for child in children:
                    state= self.scan_children_for_error(child,state)
                    if state != "PASS":
                        return 'FAIL'
        else:
            if 'result' in element:
                # explicit result
                state= element['result'].get('status',"PASS")
                return state
            else:
                # if no result : PASS is default
                state= 'PASS'
        return state




    def _iter_data(self):
        """


        :return:
        """
        cursor= self.data
        assert isinstance(cursor,list) ,"must be a list"
        assert len(cursor) == 1 , "must have only one element"
        cursor= cursor[0]
        assert cursor['suite'] , "first element must be a suite"
        next= None
        while 1:
            # extract suite
            suite= cursor.get('suite',None)
            if suite:
                children_status=self.scan_children_for_error(cursor)
                next= cursor.pop('body')
                cursor['children_status']= children_status
                yield 'suite' , cursor ,0
            else:
                yield 'NO-SUITE' , cursor ,0

            tests_data= next
            for test_data in tests_data:
                children_status= self.scan_children_for_error(test_data)
                test= test_data['test']
                if test:
                    body= test_data.pop('body')
                    test_data['children_status']= children_status
                    yield 'test', test_data ,0

                    for keyword in body:
                        for a,b, c in self._iter_inner_keyword(keyword):
                            yield a , b ,c
                    # # iterate with keywords
                    # #level= 0
                    # keywords= next
                    # for keyword in keywords:
                    #     level= 0
                    #     if 'body' in keyword:
                    #         # keyword has sub keyword
                    #         cursor= keyword
                    #         while 'body' in cursor:
                    #             level= level +1
                    #             next= cursor.pop('body')
                    #             yield 'keyword', cursor, level
                    #             cursor= next
                    #     else:
                    #         yield 'keyword', keyword , level
                    # pass


            break

    def _iter_inner_keyword(self,keyword,level=0):
        """
            iter throught keyword with body

        :param keyword:
        :param level:
        :return:
        """
       # extract body
        if 'body' in keyword:
            # keyword has a body
            body= keyword.pop('body')
            # yield self
            yield 'keyword' , keyword ,level
            level = level + 1

            # explore body
            if body:
                for next_keyword in body:
                    for a,b,c in self._iter_inner_keyword(next_keyword,level):
                        yield a , b , c
        else:
            # no body
            yield 'keyword' ,keyword, level


    def convert_time(self,data,fields=None ):
        """

        :param data:
        :return:
        """
        fields= fields or ['started','ended','timestamp']
        for field in data:
            if field in fields:
                if isinstance(data[field],int) or isinstance(data[field],float):
                    data[field]= convert_timestamp(data[field])
        return data

    def suite_header(self,data):
        """

        :param data:
        :return:
        """
        data['generator']= "Restop-Blockly"
        data['source']= data['suite']
        data['name']= data['id']
        self._last_suite_data= data
        xml= Template(xml_suite_header).render(**data)
        #print "<!--suite_header-->"
        self.out(xml)

    def suite_footer(self):
        """

        :param data:
        :return:
        """
        # copy status from children status
        self._last_suite_data['status']= self._last_suite_data.get('children_status','PASS')
        self.out("<!--suite footer-->")
        xml= Template(xml_suite_footer).render(**self._last_suite_data)
        self.out(xml)


    def test_header(self,data):
        """

        :param data:
        :return:
        """
        self.count_test += 1
        data['name']= data['id']
        data['critical'] = 'no'
        self._last_test_data= data
        self.out("<!--test_header-->")
        xml= Template(xml_test_header).render(**data)
        self.out(xml)

    def test_footer(self):
        """

        :param data:
        :return:
        """
        self.count_test -= 1
        # close remaining keywords
        while (self.count_keywords > 0):
            self.keyword_footer()

        self._last_test_data['status'] = self._last_test_data.get('children_status','PASS')

        self.out("<!--test footer-->")
        xml = Template(xml_test_footer).render(**self._last_test_data)
        self.out(xml)


    def keyword_header(self,data,level=0):
        """

        :param data:
        :param level:
        :return:
        """
        self.count_keywords += 1
        started= data.get('started',time.time())
        ended= data.get('ended',time.time())
        data['msgs']= data.get('msgs',[])
        if data['msgs'] is None:
            data['msgs']= []
        data['arguments']= data.get('arguments',[])
        data['doc']= data.get('doc',"keyword documentation")
        data['library']= data.get('library',"restop_client")

        data['status'] = dict(status='PASS', starttime=started ,endtime=ended)

        # convert message timestamp
        for msg in data['msgs']:
            self.convert_time(msg)

        if 'result' in data:
            data['status']['status']= data['result'].get('status',"PASS")
        self.out("<!--keyword header-->")
        self._last_keyword_data= data
        xml= Template(xml_keyword_header).render(**data)
        self.out(xml)

    def keyword_footer(self):
        """

        :param data:
        :return:
        """
        self.count_keywords -= 1
        self.out("<!--keyword footer-->")
        xml= Template(xml_keyword_footer).render(** self._last_keyword_data)
        self.out(xml)


    def gen_xml(self):
        """

        :return:
        """
        state= None
        last_level= 0


        for kind, data,level in self._iter_data():
            # convert timestamp
            self.convert_time(data)
            if state == None:
                # wait for suite
                assert kind == 'suite' , "must begin with a suite"
                self.suite_header(data)
                state= 'suite'

            elif state == 'suite':
                # wait for test
                assert kind == 'test' , 'must be a state'
                self.test_header(data)
                state= 'test'

            elif state == 'test':

                # wait for keywords
                if kind == 'keyword':
                    if level <= last_level and self._last_keyword_data is not None:
                        # close previous
                        for i in xrange(0,last_level-level+1):
                            self.keyword_footer()
                    else:
                        # this keyword has embeded keywords
                        #state= 'keyword'
                        pass
                    # open the keyword
                    self.keyword_header(data, level=level)

                elif kind == 'test':

                    # close previous test
                    self.test_footer()
                    self._last_keyword_data= None
                    # begin a new test
                    self.test_header(data)
                else:
                    raise RuntimeError("unexpected kind %s in state: test" % kind)

            elif state == 'keyword':
                # enbeded keyword
                if kind == 'keyword':
                    if level == last_level and self._last_keyword_data is not None:
                        # close previous keywork
                        self.keyword_footer()
                    # open keyword
                    self.keyword_header(data)

                elif kind == 'test':
                    # close current test
                    self.test_footer()
                    # open test
                    self.test_header(data)

            # save level
            last_level= level

        # end
        self.test_footer()
        self.suite_footer()

        return


    def out(self,line):
        """

        :param line:
        :return:
        """
        self._xml.append(line)
        if self.debug:
            print line


    def xml(self):
        """

        :return:
        """
        return "\n".join(self._xml)


    def html(self):
        """

        :return:
        """
        result= './output.xml'

        #r = rebot('../tests/samples/output.xml', )

        # write output.xml
        content= self.xml()
        with codecs.open(result,"w",encoding='utf-8') as fh:
            fh.write(self.xml())

        # generate log.html
        with open('stdout.txt', 'w') as stdout:
            rebot(result, name='Restop', stdout=stdout)

        # load html
        with open('log.html','r') as fh:
            self._html= fh.read()

        return self._html


    # def get_keyword(self,data):
    #     """
    #
    #     :param data:
    #     :return:
    #     """
    #     vars= xml_keyword_defaults
    #     vars.update(data)
    #
    #     t= Template(xml_keyword)
    #     text= t.render(**vars)
    #
    #     t2= Template(xml_keyword_inner)
    #     text= t2.render(**vars)



