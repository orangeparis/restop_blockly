"""
    represent a result


    value
    status
    logs
    tb
    type


"""


class ApiResponse(object):
    """



    """
    def __init__(self):
        """

        :param value:
        :param status:
        :param logs:
        :param type:
        """
        self._result= self.set(False)

    def set(self, value, status=None, logs=None, type=bool, tb=None):
        """

        :return:
        """
        logs = logs or []
        tb = tb or ''
        self._result = dict(
            value=value,
            type=type(value),
            status=status,
            logs=logs,
            tb=tb
        )
        return self._result


    @property
    def value(self):
         """

         :return:
         """
         return self._result['value']

    @property
    def type(self):
         """

         :return:
         """
         return self._result['type']




if __name__=="__main__":



    r= ApiResponse()

    print r.value
    print r.type

    r.set({'k':"value"})
    print r.value
    print r.type


    print "Done"



