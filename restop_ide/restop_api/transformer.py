"""

    transform a python code from restop blocky ide

    to a runnable code for python restop_api


"""
import time
import ast
from astor import codegen
#from ast import *
#from restop_ide.restop_api import get_api
#api= get_api()

sample='''\

unittestResults = None
device = None
timeout = None

def unit(device, timeout):
  """ this is it """
  restopResults = []
  with Session(members=[device]):
    sync( device, timeout=timeout )
    expect( device, pattern='tv', timeout=5, cancel_on='', regex='no' )
  restopResults = None


unittestResults = []
with Runner():
  unit('tv', 10)
  unit('tv2', 10)

'''


# functions that need to prefixed with api
#special_functions=['Runner','Session','Suite','open_session','sleep','log']
#special_functions.extend(['sync','expect','watch','power_on','send'])


ctx_header= '''\
from restop_ide.restop_api import Api

run_id="%s"
api = Api(run_id=run_id)


'''


def ctx_enter(section='', **kwargs ):
    """

    :param section:
    :param kwargs:
    :return:
    """
    complement=''
    if kwargs:
        complement= [',%s="%s"' % (key,kwargs[key]) for key in kwargs.keys()]
        complement= " ".join(complement)
    expr= 'api._enter("%s"%s)\n' % (section,complement)
    return ast.parse(expr).body[0]

def ctx_leave(section=''):
    expr= 'api._leave("%s")' % section
    return ast.parse(expr ).body[0]


local_functions=[]


def get_runnable(source,run_id=None):
    """

    :param source:
    :return:
    """



    run_id= run_id or str(int(time.time()))

    source= ctx_header % run_id + source

    tree = ast.parse(source)

    # visit tree to list local functions
    visitor= RestopNodeVisitor()
    visitor.visit(tree)
    global local_functions
    local_functions= list(visitor._local_functions)

    # apply code modifications
    tree = RestopNodeTransformer().visit(tree)

    modified_source = codegen.to_source(tree)

    return modified_source


def run_python_source(source):
    """

    :param python_source:
    :return:
    """
    tree = ast.parse(source)
    code = compile(tree, filename="<ast>", mode="exec")
    exec (code)
    return





class RestopNodeVisitor(ast.NodeVisitor):
    """
        vidsit code to identify local functions

    """
    def __init__(self):
        """

        """
        self._local_functions=set()

    def visit_FunctionDef(self,node):
        """
            add local function name to list
        :param node:
        :return:
        """
        self._local_functions.add(node.name)

        return node




class RestopNodeTransformer(ast.NodeTransformer):
    """

    """

    def visit_Module(self,node):
        """

        :param node:
        :return:
        """
        self._with_in= None
        self._indef = None
        return self.generic_visit(node)

    def visit_FunctionDef(self,node):
        """

        :param node:
        :return:
        """
        self._indef= node.name
        node= self.generic_visit(node)
        self._indef= None
        return node


    def visit_While(self, node):
        """
            add ctx.enter() before the while and ctx.leave() after
        :param node:
        :return:
        """
        newnode= []
        newnode.append(ctx_enter("while"))

        self.generic_visit(node)
        newnode.append(node)

        newnode.append(ctx_leave("while"))

        #ast.copy_location(newnode, node)
        #ast.fix_missing_locations(newnode)
        return newnode

    def visit_If(self, node):
        """
            add ctx.enter() before the if and ctx.leave() after
        :param node:
        :return:
        """
        source = codegen.to_source(node)
        if '\n' in source:
            source= source.split('\n')[0]

        newnode= []
        newnode.append(ctx_enter("if",line=source))

        newnode.append(node)
        self.generic_visit(node)

        newnode.append(ctx_leave("if"))

        #ast.copy_location(newnode, node)
        #ast.fix_missing_locations(newnode)
        return newnode

    def visit_For(self, node):
        """
            add ctx.enter() before the if and ctx.leave() after
        :param node:
        :return:
        """
        newnode= []
        newnode.append(ctx_enter("for"))

        newnode.append(node)
        self.generic_visit(node)

        newnode.append(ctx_leave("for"))

        #ast.copy_location(newnode, node)
        #ast.fix_missing_locations(newnode)
        return newnode


    def visit_With(self,node):
        """
        FunctionDef(self,name, args, body, decorator_list, returns):
            :param name:
            :param args:
            :param body:
            :param decorator_list:
            :param returns:
            :return:
        """
        #name= node.value.func.id
        self._with_in= node.context_expr.func.id
        self.generic_visit(node)
        self._with_in= None
        return node



    # def visit_FunctionDef(self,node):
    #     """
    #     FunctionDef(self,name, args, body, decorator_list, returns):
    #         :param name:
    #         :param args:
    #         :param body:
    #         :param decorator_list:
    #         :param returns:
    #         :return:
    #     """
    #     #name= node.value.func.id
    #     self.generic_visit(node)
    #     return node

    def visit_Call(self,node):
        """

        :param node:
        :return:
        """
        name = node.func.id

        source = codegen.to_source(node)

        if name in ['Session','Runner','Suite','set_dry_mode','last_result']:
            # transform Session(..) in api.Session(...)
            source = 'api.%s' % source
            node = ast.parse(source).body[0]
            node = node.value
        elif self._with_in == 'Session' or self._indef:
            # transform func(...) in result=api.func(...) when in a Session block
            if name not in local_functions:
                # name is not a local defined function
                source = 'result= api.%s' % source
                node= ast.parse(source).body[0]
        elif self._with_in == "Suite":
            # transform unit(..) api._enter('test'); unit(...); api._leave('test') when in with Suite block
            #newnode = []
            #newnode.append(ctx_enter("test"))
            #n= self.generic_visit(node)
            #newnode.append(node)
            #newnode.append(ctx_leave("test"))
            #ast.copy_location(newnode, node)
            #ast.fix_missing_locations(newnode)
            #return newnode

            source = codegen.to_source(node)
            new_source = "with api.Runner():\n  %s" % source
            node = ast.parse(new_source).body[0]


        else:
            # else do nothing
            pass

        return node



if __name__=="__main__":

    new_source= get_runnable(sample)
    print new_source


    run_python_source(new_source)



    # tree = ast.parse(sample)
    #
    # # ctx_enter= ast.parse(ctx_enter).body[0]
    # # ctx_leave= ast.parse(ctx_leave).body[0]
    #
    #
    # # ctx_enter= ast.parse('api._enter()').body[0]
    # # ctx_leave= ast.parse('api._leave()').body[0]
    #
    # call_function= ast.parse('open_session(first,members=[])').body[0]
    # source= codegen.to_source(call_function)
    #
    # source= 'apii.%s' % source
    # modified_method= ast.parse(source).body[0]
    #
    #
    # call_method= ast.parse('apii.open_session(first,members=[])').body[0]
    #
    # print codegen.to_source(call_method)
    #
    #
    # # apply code modifications
    # tree = RestopNodeTransformer().visit(tree)
    #
    # # Add lineno & col_offset to the nodes we created
    # #ast.fix_missing_locations(tree)
    # #exec(compile(tree, filename="<ast>", mode="exec"))
    # print "===================================="
    # modified_source= codegen.to_source(tree)
    # print modified_source
    # print "===================================="
    #
    #
    # class Context(object):
    #
    #     def enter(self):
    #         print('enter')
    #     def leave(self):
    #         print('leave')
    #
    # ctx= Context()
    #
    # def open_session(members):
    #     print("open_session")
    #
    # def send(alias,cmd):
    #     print("send")

    # tree = ast.parse(new_source)
    # code= compile(tree, filename="<ast>", mode="exec")
    #
    # exec(code)




    print "Done"


