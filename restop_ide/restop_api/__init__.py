import os
import time
import sys
import threading
from pprint import pprint
import walrus

from restop_client.http_proxy import SimpleProxy
#from restop_ide import config

from tracer import YamlTracer
from result_handler import ApiResponse


import logging

log=logging.getLogger('restop')

# try:
#     base_url= config.RESTOP_MASTER_URL
# except AttributeError:
#     base_url= "http://localhost:5000/restop/api/v1"


redis_host= os.environ.get('REDIS_HOST','0.0.0.0')
redis_port= int(os.environ.get('REDIS_PORT', 6379 ))
redis_db= int(os.environ.get('REDIS_DB', 0))

base_url= os.environ.get('RESTOP_MASTER_URL', "http://localhost:5000/restop/api/v1")


redis_database= walrus.Database(host=redis_host,port=redis_port, db=redis_db)


# Based on tornado.ioloop.IOLoop.instance() approach.
# See https://github.com/facebook/tornado
class SingletonMixin(object):
    __singleton_lock = threading.Lock()
    __singleton_instance = None

    @classmethod
    def instance(cls):
        if not cls.__singleton_instance:
            with cls.__singleton_lock:
                if not cls.__singleton_instance:
                    cls.__singleton_instance = cls()
        return cls.__singleton_instance


def set_dry_mode(mode=False):
    """

    :param mode:
    :return:
    """
    api= Api.instance()
    api._dry_mode= mode



class Session(object):
    """


    """
    def __init__(self,members,** kwargs):
        """

        :param members:
        """
        #self.api = get_api()
        self.api=Api.instance()
        self.members= members
        self.kwargs= kwargs
        self.session=None

    def __enter__(self):
        """

        :return:
        """
        print "enter session"
        #self.session= self.api.open_session(self.members)

        try:
            self.session = self.api.open_session(self.members)
        except RuntimeError,e:
            self.api.close_session()
            raise
        except Exception,e:
            print('failed to open session: %s' % str(e))
            self.api.close_session()
            raise
        return self

    def __exit__(self, type, value, traceback):
        """

        :param type:
        :param value:
        :param traceback:
        :return:
        """
        print "leave session"
        self.api.close_session()
        sys.exc_clear()


class Runner(object):
    """


    """
    def __init__(self,** kwargs):
        """

        :param members:
        """
        #self.api = get_api()
        self.api= Api.instance()
        self.kwargs= kwargs

        self._dry_mode = False

    def __enter__(self):
        """

        :return:
        """
        self.api._tracer.testcase_start(name='restop')
        print "enter Test"
        return

    def __exit__(self, type, value, traceback):
        """

        :param type:
        :param value:
        :param traceback:
        :return:
        """
        self.api._tracer.testcase_end()
        print "leave Test"
        sys.exc_clear()


class Suite(object):
    """


    """
    def __init__(self,** kwargs):
        """

        :param members:
        """
        #self.api = get_api()
        self.api = Api.instance()
        self.kwargs= kwargs

    def __enter__(self):
        """

        :return:
        """
        self.api._tracer.suite_start(name='restop')
        print "enter Suite"
        return

    def __exit__(self, type, value, traceback):
        """

        :param type:
        :param value:
        :param traceback:
        :return:
        """
        self.api._tracer.suite_end()
        print "leave Suite"
        sys.exc_clear()


class Api(SingletonMixin):
    """

        api to restop client

    """
    def __init__(self,logger=None,run_id=None):
        """

        :param logger:
        """
        # set this instance as the singleton instance
        self.__class__._SingletonMixin__singleton_instance= self

        self.run_id= run_id
        self._tracer= YamlTracer(run_id=run_id,redis_db=redis_database)
        #self.log= logger or log
        self._last_response= {}
        #self._result = Result()
        self._dry_mode= False
        self.setup_pilot()

    Session= Session
    Runner= Runner
    Suite= Suite


    def setup_pilot(self,platform_name=None,platform_version=None,platform_url=None,dry_mode=False):
        """
            initialize pilot with platform configuration file
        """
        self._platform_name= platform_name or 'local'
        self._platform_version= platform_version or 'v1'
        self._platform_url= platform_url or base_url
        self._dry_mode= dry_mode

        print "pilot: setting up platform pilot for platform=%s , version=%s ,url=%s" % (
            platform_name,platform_version,platform_url)

        collection_operations= ['adb_devices','get_platform_configuration']
        self._proxy= SimpleProxy(
                self._platform_url,
                collection= 'sessions',
                collection_operations= collection_operations
                )
        return

    def get_platform_configuration(self):
        """
            Get platform configuration ( a platform.json)
        """
        if not self._dry_mode:
            result=self.proxy.get_platform_configuration()
            print "pilot: platform configuration is %s" % str(result)
            return result
        else:
            return self._dry_return()

    # def open_Session(self, *users):
    #     """
    #         open a session : start session with a terminal per user specified
    #
    #         Open Session Alice Bob
    #
    #         -    start a session with userA=Alice , userB=Bob
    #     """
    #     print "pilot: initialisation conf = %s " % str(self.conf)
    #     print "pilot: Open Session with users: %s" % str(users)
    #
    #     if self._dry:
    #         return self._dry_return()
    #
    #     result = self.proxy.open(users)
    #     # {u'agents_count': 2, u'agent_started': 2}
    #     pprint(result)
    #     # check all agents are started
    #     if result.has_key('code') and result['code'] >= 300:
    #         raise RuntimeError('cannot open session')
    #     if not result['agent_started'] == result['agents_count']:
    #         raise RuntimeError("cant start all agents")

    def open_session(self,members=None):
        """

        :param members:
        :return:
        """
        members= members or []
        self._indent=0
        #self.log=log
        started= time.time()
        self._tracer.keyword_start('open_session',started=started)
        self._tracer.keyword_msg(content="open session with members: %s" % str(members))
        # call open session
        if self._dry_mode:
            self._dry_return()
            self._tracer.keyword_end()
        else:
            # run open session
            try:
                result = self._proxy.open(members)
            except Exception,e:
                self._tracer.keyword_msg(str(e),level='ERROR')
                self._tracer.keyword_status('FAIL')
                self._tracer.keyword_end()
                raise
            pprint(result)

            # check all agents are startedr
            if result.has_key('result') and result['result'] >= 300:
                self._tracer.keyword_msg('cannot open session',level='ERROR')
                if 'message' in result:
                    self._tracer.keyword_msg(result['message'],level='DEBUG')
                self._tracer.keyword_status('FAIL')
                self._tracer.keyword_end()
                raise RuntimeError('cannot open session')
                #return False

            # session OK
            self._tracer.keyword_msg('session open', level='INFO')
            if 'message' in result:
                self._tracer.keyword_msg(str(result['message']), level='DEBUG')
            if 'logs' in result:
                self._tracer.keyword_msg(str(result['logs']), level='DEBUG')
            self._tracer.keyword_status('PASS')
            self._tracer.keyword_end()

            # if not result['agent_started'] == result['agents_count']:
            #     self._tracer.keyword_msg('cant start all agents',level='ERROR')
            #     if 'message' in result:
            #         self._tracer.keyword_msg(result['message'], level='DEBUG')
            #     self._tracer.keyword_status('FAIL')
            #     self._tracer.keyword_end()
            #     raise RuntimeError("cant start all agents")
                #return False
        return {}
            #self._tracer.keyword_msg("my content\n with < problematic \n characters")
            #self._tracer.keyword_status(status='PASS',started=started,critical='yes')


    def close_session(self):
        """

        :return:
        """
        self._tracer.keyword_start('close_session')

        if self._dry_mode:
            # dry mode
            self._dry_return()
            self._tracer.keyword_end()
        else:
            # real close
            result = self._proxy.close()
            self._tracer.keyword_msg('result is: %s' % result['message'])
            self._tracer.keyword_msg('logs:\n%s' % str(result['logs']),level='DEBUG')
            self._tracer.keyword_end()

            # print r['logs'
            # print("close_session()")
            # self._tracer.keyword_end()

    def sleep(self,timeout=1):
        """

        :param timeout:
        :return:
        """
        self._tracer.keyword_start('sleep')
        self._tracer.keyword_msg('sleep %d second(s)' % timeout)
        print "sleep(1)"
        time.sleep(timeout)
        self._tracer.keyword_end()

    def log(self,message='No log'):
        """

        :param message:
        :return:
        """
        self._tracer.keyword_start('log')
        self._tracer.keyword_msg(str(message))
        self._tracer.keyword_status('PASS')
        self._tracer.keyword_end()

    def exit(self,message='exit',status='FAIL'):
        """

        :param message:
        :param status:
        :return:
        """
        if isinstance(message,Exception):
            if isinstance(message,RuntimeError):
                return
            message= str(message)

        self._tracer.keyword_start('exit')
        self._tracer.keyword_msg(str(message))
        self._tracer.keyword_status(status)
        self._tracer.keyword_end()


    def _abort(self,message="abort"):
        """

        :param message:
        :return:
        """
        raise RuntimeError(message)

    def abort(self,message='user abort'):
        """

        :param message:
        :return:
        """
        self._tracer.keyword_start('abort')
        self._tracer.keyword_msg(str(message))
        self._tracer.keyword_status('FAIL')
        self._tracer.keyword_end()
        self._abort(message)



    def set_dry_mode(self,mode=False):
        """

        :param mode:
        :return:
        """
        #self._tracer.keyword_start('dry_mode')
        #self._tracer.keyword_msg('set dry mode to %s' % mode)
        if mode is not True and mode is not False:
            mode= False
        self._dry_mode= mode
        #self._tracer.keyword_status('PASS')
        #self._tracer.keyword_end()

    def last_result(self):
        """

        :return:
        """
        return self.result

    @property
    def result(self):
        """

        :return:
        """
        try:
            return self._last_response['message']
        except Exception,e:
            return None

    def result_should_be(self,value):
        """

        :param value:
        :return:
        """
        #assert self._result['value'] == value
        self._tracer.keyword_start('result_should_be')
        actual_value = self.result
        self._tracer.keyword_msg("result value should be [%s] => got [%s]" %
                                 (str(value),str(actual_value)))
        try:
            assert self.result == value
            self._tracer.keyword_status('PASS')
            self._tracer.keyword_end()
        except Exception,e:
            self._tracer.keyword_status('FAIL')
            self._tracer.keyword_end()
            self._abort()

    def _result_value_for_key(self,key):
        """

        :param key:
        :return:
        """
        try:
            return self.result[key]
        except Exception,e:
            return None


    def result_value_for_key_should_be(self,key,value):
        """

        :param key:
        :param value:
        :return:
        """
        self._tracer.keyword_start('result_value_for_key_should_be')
        actual_value = self._result_value_for_key(key)
        self._tracer.keyword_msg("result value for key %s should be %s => %s" %
                                 (str(key),str(value),str(actual_value)))
        try:
            assert actual_value == value
            self._tracer.keyword_status('PASS')
            self._tracer.keyword_end()
        except Exception, e:
            self._tracer.keyword_result('FAIL')
            self._tracer.keyword_end()
            self._abort()


    def _enter(self,section,**kwargs):
        """

        :param section:
        :param kwargs:
        :return:
        """
        print "_enter(%s)" % section
        self._tracer.keyword_start(section,body=True)
        # if kwargs:
        #     for key,value in kwargs.iteritems():
        #         content= "%s: %s " % (key,value)
        #         self._tracer.keyword_msg(content)



    def _leave(self,section):
        """

        :return:
        """
        print "_leave(%s)" % section
        self._tracer.keyword_end()
        #self._tracer.out("ended: %d\n" % time.time(),indent=self._indent)
        #self._indent=self._indent -2

    def _out(self,msg='.'):
        """
            display on stdout
        :param msg:
        :return:
        """

    def _log(self,msg):
        """

        :param msg:
        :return:
        """


    def _dry_return(self,*args,**kwargs):
        """
            return with dry mode : always OK
        """
        print "pilot: warning you are in dry mode, no operation was transmitted to api"
        result= dict(status=200,message=True,logs=['WARNING you are in dry mode',"keyword has been skipped"])
        return self._handle_result(result)


    #
    # handle wild card
    #
    # def _execute(self):
    #     pass


    def _handle_result(self, result, raise_on_error=True,indent=0):
        """

        :param result: dict   { result: 200 , logs: [] , message: ""}
        :return:
        """
        if isinstance(result,dict):
            # normal case
            status = result.get('result', 200)
            message = result.get('message', True)
            logs= result.get('logs', [])
            tb= result.get('tb',"")
        else:
            # exceptional case
            status= 500
            message= "unexpected error"
            logs= [result]
            tb= ""

        # convert status
        if status < 200 or status >= 300:
            status= "FAIL"
        else:
            status= "PASS"

        self._tracer.keyword_msg("result is : %s" % message)
        self._tracer.keyword_msg("logs:\n%s" % str(logs),level='DEBUG')
        self._tracer.keyword_status(status)
        self._tracer.keyword_end()
        print '=== status:%s' % str(status)
        print '=== message: %s' % 'message'
        print '=== logs:'
        pprint(logs)
        if tb:
            print("=== trace-back:")
            pprint(tb)

        # check status
        if raise_on_error:
            if status == 'FAIL':
                raise RuntimeError("FAIL: %s" % str(message))
        return message




    # primary interface
    def _execute(self, alias, command, *args, **kwargs):
        """
           the transmission belt to the proxy

        """
        message= None
        self._tracer.keyword_start(command)

        if self._dry_mode:
            # dry mode: dont execute

            print("dummy execute %s %s %s %s" % (alias, command, str(args), str(kwargs)))
            time.sleep(1)
            self._dry_return()
            self._tracer.keyword_end()

        else:
            # run the real command
            result= self._proxy._send_operation(command,alias,**kwargs)

            self._last_response= result
            message= self._handle_result(result)

        #self._tracer.keyword_end()

        return message


        # if not self._dry:
        #     # return self._session.execute(user,command,*args,**kwargs)
        #     res = self.proxy._send_operation(command, user=user, **kwargs)
        #     if not isinstance(res,dict):
        #         # res is text
        #         res= dict(message='failed',result=500,logs=[res])
        #     return self._handle_result(res)
        #
        # else:
        #     return self._dry_return()

    # mapping for the agent method ( click , wait_for_exists , press_home ... )
    def __getattr__(self, function_name):
        """ wrapper to wild methods
        :param function_name:
        :return:
        """
        def wrapper(alias='-', **kwargs):
            return self._execute(alias, function_name, **kwargs)
        return wrapper