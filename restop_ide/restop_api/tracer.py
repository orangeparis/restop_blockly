import time
import walrus


#redis_db= walrus.Database()


class YamlTracer(object):
    """
        write elements to a trace file


    """
    def __init__(self,name="script",filename="trace.yml",run_id=None,redis_db=None):
        """


        :param name:
        :param filename:
        """
        self.name=name
        self.filename=filename
        self.run_id= run_id or str(int(time.time()))
        redis_db= redis_db or walrus.Database()

        self._indent= 0
        self._step_indent= 2
        self._suite_counter=0
        self._test_counter=0

        msg_start= "# start of trace\n\n"
        # open file log
        with open(self.filename,"w") as fh:
            fh.write(msg_start)

        # initialize redis log
        self.rlog= walrus.List(redis_db,"task:container:trace:task:id:%s" % self.run_id)


    def set_indent(self,indent=0):
        self._indent= indent

    def indent(self,step=None):
        step=step or self._step_indent
        self._indent=self._indent + step

    def deindent(self,step=None):
        step= step or self._step_indent
        self._indent=self._indent - step
        if self._indent <= 0:
            self._indent= 0


    def _out(self,line):
        """
        low level out to trace
        :param line:
        :return:
        """
        # log to file
        with open(self.filename, "a") as fh:
            fh.write(line)
        # log to redis
        self.rlog.append(line)

    def out(self,message,indent=0):
        """
        high level out line ( with indent )
        :param message:
        :param indent:
        :return:
        """
        indent= indent or self._indent
        indent= " " * indent
        line= indent + message
        self._out(line)


    def out_multiline(self,message,indent=0):
        """

        :param message:
        :param indent:
        :return:
        """
        indent= indent or self._indent
        if "\n" in message:
            for line in message.split('\n'):
                self.out(line + '\n',indent=indent)
        else:
            self.out(message + '\n', indent=indent)



        # with open(self.filename,"a") as fh:
        #     fh.write(line)

    def keyword_start(self,keyword,body=False,started=None):
        """

        :param keyword:
        :param body:
        :return:
        """
        started= started or time.time()
        self.out("- keyword: %s\n" % keyword)
        self.indent()
        self.out("started: %d\n" % started)
        if body:
            # a block
            self.out("body:\n")
        else:
            # a keyword
            self.out("msgs:\n")

    def keyword_msg(self, content, level="INFO"):
        """
            generate a message output

            - level: INFO
              content: |
                dsdsdsdsdsdsds

        :param content:
        :param level:
        :return:
        """
        self.out("- level: %s\n" % level)
        self.indent()
        self.out('timestamp: %d\n' % time.time())
        self.out("content: |\n")
        self.indent(10)
        self.out_multiline(content)
        self.deindent(10)
        self.deindent()

    def keyword_status(self,status= None):
        """

        :param data:
        :return:
        """
        status= status or 'PASS'
        #starttime= started or time.time()
        #endtime= ended or time.time()

        self.out("result:\n")
        self.indent()
        self.out("status: %s\n" % str(status))
        # if critical:
        #     self.out("critical: %s\n" % critical)
        self.deindent()
        return ()

    def keyword_end(self,keyword=''):
        """

        :param keyword:
        :return:
        """
        self.out("ended: %d\n" % time.time())
        self.deindent()

    def out_logs(self,data):
        """

        :param data:
        :return:
        """
        self.out('logs: |\n')
        indent= self._indent + self._step_indent
        for line in data:
            if not line.endswith('\n'):
                line=line+'\n'
            self.out(line,indent=indent)
        return


    def keyword_result(self,status,message='no message',logs=None,tb=None):
        """

        :param status:
        :param message:
        :param logs:
        :param tb:
        :return:
        """
        self.out("result:\n")
        self.indent()
        self.out("status: %s\n" % str(status))
        self.out("message: %s\n" % message)
        if logs:
            #log_string= "\n".join(logs)
            self.out_logs(logs)
        self.deindent()
        return()


    def testcase_start(self,id=None,name='test'):
        """

        :param id:
        :param name:
        :return:
        """
        self.set_indent(2)
        self._test_counter = self._test_counter + 1
        id = id or "s%d-t%d" % (self._suite_counter, self._test_counter)

        self.out("- test: %s\n" % name)
        self.indent()
        self.out("started: %d\n" % time.time())
        self.out("id: %s\n" % id)
        #self.out("name: %s\n" % name)
        self.out("body:\n")

    def testcase_end(self, name=''):
        """

        :param keyword:
        :return:
        """
        self.out("ended: %d\n" % time.time())
        self.deindent()


    def suite_start(self, id=None, name='suite',source=''):
        """

        :param id:
        :param name:
        :return:
        """
        self.set_indent(0)
        self.rlog.append("# start suite\n\n")

        self._suite_counter= self._suite_counter + 1
        id= id or "s%d" % self._suite_counter

        self.out("- suite: %s\n" % name)
        self.indent()
        self.out("started: %d\n" % time.time())
        self.out("id: %s\n" % id)
        self.out("source: %s\n" % source)
        self.out("body:\n")

    def suite_end(self, name=''):
        """

        :param keyword:
        :return:
        """
        self.out("ended: %d\n" % time.time())
        self.deindent()

    def report_start(self,name='report' ,generator='Restop-Blockly'):
        """
                - report: name
                  generator:

        :return:
        """
        self.out("- report: %s\n" % name)
        self.indent()
        self.out("generated: %d\n" % time.time())
        self.out("generator: %s\n" % generator)
        self.out("body:\n")

    def report_end(self):
        """

        :return:
        """
        self.out("ended: %d\n" % time.time())
        self.deindent()