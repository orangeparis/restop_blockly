# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))


BLOCKY_APP_HOST= "0.0.0.0"
BLOCKLY_APP_PORT= 5016
BLOCKLY_APP_DEBUG= True

BLOCKLY_CATEGORIES= ['samples','livebox','tvbox']

RESTOP_STATION_IP= 'localhost'

# distant restop platform
RESTOP_MASTER_URL= 'http://%s:5000/restop/api/v1' % RESTOP_STATION_IP
#RESTOP_MASTER_URL= 'http://192.168.1.21/restop/api/v1'
RESTOP_PLATFORM_NAME= 'default'

# Define the database - we are working with
REDIS_HOST='localhost'
REDIS_PORT= 6379
REDIS_DB= 0

REDIS_COMMANDER_HOST= '192.168.99.100'
REDIS_COMMANDER_PORT= 8081

GRAFANA_HOST= '192.168.99.100'
GRAFANA_PORT= 3000


# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"