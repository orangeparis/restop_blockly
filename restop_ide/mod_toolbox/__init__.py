import os
import json
from flask import Blueprint, render_template, abort,request,current_app
from jinja2 import TemplateNotFound
from generate_restop_blocks import Methods

toolbox = Blueprint('toolbox', __name__,template_folder='templates')

template_dir=os.path.join(os.path.dirname(__file__),"templates")


def canvas_list(directory):
    """


    :return:
    """
    for filename in os.listdir(directory):
        extension = os.path.splitext(filename)[1]
        if extension.lower() == '.xml':
            if filename.lower() != 'default.xml':
                yield filename[:-4]



@toolbox.route('/toolbox')
def root():
    """

    :return:
    """
    canvas_directory= os.path.join(template_dir,'canvas')
    canvases= list(canvas_list(canvas_directory))
    categories= current_app.config.get('BLOCKLY_CATEGORIES',['samples'])
    #categories= ['samples','tvbox','livebox']
    methods={}
    try:
        for name in categories:
            filename= os.path.join(template_dir,"%s_data.json" % name)
            with open(filename,'rb') as fh:
                json_data= fh.read()
                data= json.loads(json_data)
                methods[name]= Methods(data['item_methods'])

        #response = render_template("ide.html", methods=methods)
        response = render_template(
            "new_toolbox.html",
            methods=methods,
            categories=categories,
            canvas_list= canvases
        )
        return response
        #return render_template('toolbox.html')
        #return render_template('ide.html')
    except TemplateNotFound,e:
        abort(404,"str(e)")

@toolbox.route('/toolbox/run/<name>',methods=['GET','PUT'])
def toolbox_run(name='default'):
    try:
        r= request
        data=r.get_data()
        backend = current_app.config['backend']
        rc= backend.run_canvas(name,data=data)
        return "run received %s" % rc
    except TemplateNotFound:
        abort(404)



@toolbox.route('/toolbox/scripts/<name>', methods=['GET'])
def toolbox_scripts(name):
    """
        get js scripts from templates
            eg unittest.js, unittest_python.js

    :param name:
    :return:
    """
    try:
        return render_template(name)
    except TemplateNotFound:
        abort(404)

@toolbox.route('/toolbox/dynablocks/<name>', methods=['GET'])
def toolbox_dynablocks(name):
    """
        generate dynamically block for the category (tvbox, livebox) ....

            eg: tvbox -> tvbox_data.json


    :param name:
    :return:
    """
    try:
        filename= os.path.join(template_dir,"%s_data.json" % name)
        with open(filename,'rb') as fh:
            json_data= fh.read()
            data= json.loads(json_data)
            methods= Methods(data['item_methods'])
            response= render_template("dynablocks.j2",methods=methods)
            return response
    except Exception,e:
        abort(404)


@toolbox.route('/toolbox/save', methods=['GET', 'PUT'])
def toolbox_save():
    """
        save data to backend

    :return:
    """
    abort(404)
    # try:
    #     r = request
    #     data = r.get_data()
    #     backend= current_app.config['backend']
    #     rc= backend.save_canvas(data)
    #     return "saved"
    # except TemplateNotFound:
    #     abort(404)


@toolbox.route('/toolbox/canvas/<name>', methods=['GET','PUT'])
def toolbox_canvas(name):
    """
        load/save a canvas for the tests  ( colour,loop2, restop )

            eg: restop_playground -> templates/canvas/restop.xml


    :param name:
    :return:
    """
    if request.method == 'GET':
        # return the canvas
        try:
            backend= current_app.config['backend']
            response= backend.load_canvas(name)
            return response
        except Exception, e:
            abort(404)
    elif request.method == 'PUT':
        # store the canvas
        try:
            r = request
            data = r.get_data()
            if len(data) >= 1800:
                # size ok save it
                backend = current_app.config['backend']
                rc = backend.save_canvas(data,name=name)
                return "saved"
            else:
                # size too low: ignore to protect overwrite
                return "too small: not saved"
        except TemplateNotFound:
            abort(404)
    abort(405)






# @toolbox.route('/', defaults={'page': 'index'})
# @toolbox.route('/<page>')
# def show(page):
#     try:
#         return render_template('toolbox.html' % page)
#     except TemplateNotFound:
#         abort(404)


@toolbox.route('/new')
def new_toolbox():
    """

    :return:
    """
    methods={}
    try:
        for name in ['tvbox','livebox']:
            filename= os.path.join(template_dir,"%s_data.json" % name)
            with open(filename,'rb') as fh:
                json_data= fh.read()
                data= json.loads(json_data)
                methods[name]= Methods(data['item_methods'])

        response = render_template("new_toolbox.html", methods=methods)
        return response
        #return render_template('toolbox.html')
        #return render_template('ide.html')
    except TemplateNotFound,e:
        abort(404,"str(e)")