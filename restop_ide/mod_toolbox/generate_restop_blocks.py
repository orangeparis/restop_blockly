# -*- coding: utf-8 -*-

#from jinja2 import Environment, PackageLoader, Template
import os
import json
from jinja2 import Environment, PackageLoader, Template

template_dir=os.path.join(os.path.dirname(__file__),"templates")


"""




"""


#
# template for xml method in categories
#
xml_block= '''\
  <block type="{{ method.type_name }}">
    {%- for field,value in method.iter_default_values() %}
      <value name="{{ field }}">
          {{ method.simple_value( value )}}
      </value>
    {%- endfor %}
  </block>
'''

#
# template for method block
#
blockly_block='''\
Blockly.Blocks['{{ method.type_name }}'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("{{ method.name  }}");
    {%- for field,field_type in method.iter_fields() %}
    this.appendValueInput("{{ field }}")
    .setCheck("{{ field_type }}")
    .appendField("{{ field }}");
    {%- endfor %}

    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};
'''

python_code='''\
Blockly.Python['{{ method.type_name}}'] = function(block) {
  var code= '{{ method.name }}( ';
  {%- for key,value in method.iter_python_arguments() %}
  code= code + '{{ key }}' + {{ value }}
  {%- endfor %}
  code= code + ' )';
  return code;
};
'''


class Methods(object):
    """


    """
    def __init__(self,data):
        """

        :param data: dict
        """
        self.data= data

    def iter_methods(self):
        """

        :return:
        """
        for method_name in sorted(self.data.keys()):
        #for method_name,method_data in self.data.iteritems():
            method_data= self.data[method_name]
            method=Method(method_name,method_data)
            yield method

    __iter__=iter_methods

class Method(object):
    """


    """
    _checks= ('String','Number','Boolean')

    def __init__(self,name,data):
        """



        :param name: string : method name
        :param data: dict : argspec,docstring
        """
        self.name=name
        self.data=data
        self.docstring= self.data.get('docstring','')
        self.argspec= self.data.get('argspec',None)

    @property
    def type_name(self):
        return 'restop_' + self.name


    def get_default_values(self):
        """

        :return:
        """
        defaults = {}
        if self.argspec['defaults']:
            defaults_offset = len(self.argspec['args']) - len(self.argspec['defaults'])
            for index, value in enumerate(self.argspec['defaults']):
                if value is not None:
                    field_name = self.argspec['args'][index + defaults_offset]
                    defaults[field_name] = value
        return defaults

    def iter_default_values(self):
        """

        :return:
        """
        for field_name,value in self.get_default_values().iteritems():
            yield field_name,value

        # defaults_offset = len(self.argspec['args']) - len(self.argspec['defaults'])
        # for index, value in enumerate(self.argspec['defaults']):
        #     if value is not None:
        #         field_name = self.argspec['args'][index + defaults_offset]
        #         yield field_name, value

    def simple_value(self, value, name=None):
        """


        :param value:
        """
        if value == None:
            return ''
        elif isinstance(value, (tuple, list)):
            return '<block type="lists_create_empty"></block>'
        elif isinstance(value, bool):
            name = name or "BOOL"
            if value:
                value = 'TRUE'
            else:
                value = 'FALSE'
            return '<block type="logic_boolean"><field name="%s">%s</field></block>' % (name, value)
        elif isinstance(value, (int, float)):
            name = name or "NUM"
            return '<block type="math_number"><field name="%s">%s</field></block>' % (name, str(value))
        else:
            # string
            name = name or 'TEXT'
            return '<block type="text"><field name="%s">%s</field></block>' % (name, unicode(value))


    def iter_fields(self):
        """

        :return:
        """
        defaults={}
        if self.argspec['defaults']:
            defaults_offset = len(self.argspec['args']) - len(self.argspec['defaults'])
            for index,value in enumerate(self.argspec['defaults']):
                if value is not None:
                    field_name= self.argspec['args'][index+defaults_offset]
                    defaults[field_name]= value
        for field in self.argspec['args']:
            field_type = 'String'
            if field == 'self':
                field= 'alias'
            else:
                if field in defaults:
                    value= defaults[field]
                    field_type= self._get_type(value)

            yield field,field_type
    __iter__= iter_fields

    def _get_type(self,value):
        """

        :param value:
        :return:
        """
        if value is None:
            return 'String'
        elif isinstance(value,(int,float)):
            return 'Number'
        elif isinstance(value,bool):
            return 'Boolean'
        else:
            return 'String'

    def iter_python_arguments(self):
        """


        :return:
        """
        pattern= "Blockly.Python.valueToCode(block, '%s', Blockly.Python.ORDER_ATOMIC);"
        for field in self.argspec['args']:
            if field == 'self':
                field= 'alias'
                key= ''
                value= pattern % field
            else:
                key= ", %s=" % field
                value= pattern % field
            yield key,value

# def gen_keyword_block(data):
#     """
#
#
#     :param data:
#     :return:
#     """
#     env = Environment(loader=PackageLoader('drafts', 'templates'))
#
#
#     for entry in data:
#
#         template = env.get_template('keywords.jinja')
#         print template.render(entry=entry)
#
#         template= env.get_template('generators.jinja')
#         print template.render(entry=entry)
#
#
#     template = env.get_template('index.html')
#     print template.render(entries=data)
#
#     return


if __name__=="__main__":




    def test_send_key():

        name= u'send_key'
        data= {u'argspec':
                   {u'keywords': None,
                    u'args':
                        [u'self', u'key', u'timeout', u'wait'],
                    u'defaults': [5, False],
                    u'varargs': None}
               }


        method =Method(name,data)

        # for field,value in m.iter_default_values():
        #     print field , value

        template= Template(xml_block)
        r= template.render(method=method)
        print r

        for field, value in method.iter_default_values():
            value= method.simple_value(value)
            continue




        template= Template(blockly_block)
        r= template.render(method=m)
        print r

        template= Template(python_code)
        r= template.render(method=m)
        print r





    def test_method():

        name= u'expect'
        data= {u'argspec': {u'args': [  u'self',
                                        u'pattern',
                                        u'timeout',
                                        u'cancel_on',
                                        u'regex'],
                                 u'defaults': [5, None, u'no'],
                                 u'keywords': u'kwargs',
                                 u'varargs': None}
               }

        m =Method(name,data)

        # for field,value in m.iter_default_values():
        #     print field , value

        template= Template(xml_block)
        r= template.render(method=m)
        print r

        template= Template(blockly_block)
        r= template.render(method=m)
        print r

        template= Template(python_code)
        r= template.render(method=m)
        print r


    def test_methods(name="tvbox"):
        """



        :return:
        """
        filename= os.path.join(template_dir,"%s_data.json" % name)
        with open(filename,'rb') as fh:
            json_data= fh.read()
            data= json.loads(json_data)
            methods= Methods(data['item_methods'])

            #for method in methods.iter_methods():
            for method in methods:
                template = Template(blockly_block)
                r = template.render(method=method)
                print r

                template = Template(python_code)
                r = template.render(method=method)
                print r

            for method in methods:
                template = Template(xml_block)
                r = template.render(method=method)
                print r


        return

    # begin
    #test_method()
    #test_methods()

    test_send_key()


    # with open('./keywords.yml', "rb") as fh:
    #     data = load(fh.read(), Loader)
    #
    #     rc = gen_keyword_block(data)


    print "Done."


