/**
 * @fileoverview Generating Python for unit test blocks.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

//goog.provide('Blockly.Python.procedures');

goog.require('Blockly.Python');


Blockly.Python['unittest_restop'] = function(block) {
  // Container for unit tests.
  var resultsVar = Blockly.Python.variableDB_.getName('unittestResults', Blockly.Variables.NAME_TYPE);
  var resultVar = Blockly.Python.variableDB_.getName('result', Blockly.Variables.NAME_TYPE);
  // var functionName = Blockly.Python.provideFunction_(
  //     'restop_report',
  //     ['def ' + Blockly.Python.FUNCTION_NAME_PLACEHOLDER_ + '():',
  //      '  # Create test report.',
  //      '  report = []',
  //      '  summary = []',
  //      '  fails = 0',
  //      '  for (success, log, message) in ' + resultsVar + ':',
  //      '    if success:',
  //      '      summary.append(".")',
  //      '    else:',
  //      '      summary.append("F")',
  //      '      fails += 1',
  //      '      report.append("")',
  //      '      report.append("FAIL: " + message)',
  //      '      report.append(log)',
  //      '  report.insert(0, "".join(summary))',
  //      '  report.append("")',
  //      '  report.append("Number of tests run: %d" % len(' + resultsVar + '))',
  //      '  report.append("")',
  //      '  if fails:',
  //      '    report.append("FAILED (failures=%d)" % fails)',
  //      '  else:',
  //      '    report.append("OK")',
  //      '  return "\\n".join(report)']);

  // Setup global to hold test results.
  var code= '';
  //code= code + 'from restop_api import get_api\n';
  //code= code + 'api= get_api()\n';
  code = code + resultsVar + ' = []\n';
  code = code + resultVar + ' = {}\n';
  code = code + 'with Suite():\n';
  // Run tests (unindented).
  code += Blockly.Python.statementToCode(block, 'DO');
      //.replace(/^  /, '').replace(/\n  /g, '\n');
  // var reportVar = Blockly.Python.variableDB_.getDistinctName(
  //     'report', Blockly.Variables.NAME_TYPE);
  //code += reportVar + ' = ' + functionName + '()\n';
  // Destroy results.
  //code += resultsVar + ' = None\n';
  // Print the report.
  //code += 'print(' + reportVar + ')\n';
  return code;
};


Blockly.Python['restop_result'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = 'last_result() ';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};



//restop
// Blockly.Python['open_session'] = function(block) {
//   var value_members = Blockly.Python.valueToCode(block, 'members', Blockly.Python.ORDER_ATOMIC);
//   var statements_operations = Blockly.Python.statementToCode(block, 'operations');
//   // TODO: Assemble Python into code variable.
//   var code = '    open_session  members=' + value_members +'\n';
//   return code;
// };

Blockly.Python['restop_alias'] = function(block) {
  var dropdown_alias = block.getFieldValue('alias');
  // TODO: Assemble Python into code variable.
    //var value_alias = Blockly.Python.valueToCode(block, 'alias', Blockly.Python.ORDER_ATOMIC);
  var code = dropdown_alias;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_ATOMIC];
  //  return code
};


Blockly.Python['restop_unit'] = function(block) {
  var value_unit_name = Blockly.Python.valueToCode(block, 'unit_name', Blockly.Python.ORDER_ATOMIC);
  var value_members = Blockly.Python.valueToCode(block, 'members', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = 'unit '+ value_unit_name + '\n' + '  open_session  members=' + value_members +'\n';
  return code;
};



//
// with blocks
//
Blockly.Python['controls_with'] = function(block) {
  // Repeat n times.
  if (block.getField('TIMES')) {
    // Internal number.
    var repeats = String(parseInt(block.getFieldValue('TIMES'), 10));
  } else {
    // External number.
    var repeats = Blockly.Python.valueToCode(block, 'TIMES',
        Blockly.Python.ORDER_NONE) || '0';
  }
  if (Blockly.isNumber(repeats)) {
    repeats = parseInt(repeats, 10);
  } else {
    repeats = 'int(' + repeats + ')';
  }
  var branch = Blockly.Python.statementToCode(block, 'DO');
  branch = Blockly.Python.addLoopTrap(branch, block.id) ||
      Blockly.Python.PASS;
  var loopVar = Blockly.Python.variableDB_.getDistinctName(
      'count', Blockly.Variables.NAME_TYPE);
  var code = 'with Session( ' + loopVar + ') as session:\n' + branch;
  return code;
};

Blockly.Python['restop_procedures'] = function(block) {
  // Define a procedure with a return value.
  // First, add a 'global' statement for every variable that is assigned.
  var globals = Blockly.Variables.allVariables(block);
  for (var i = globals.length - 1; i >= 0; i--) {
    var varName = globals[i];
    if (block.arguments_.indexOf(varName) == -1) {
      globals[i] = Blockly.Python.variableDB_.getName(varName,
          Blockly.Variables.NAME_TYPE);
    } else {
      // This variable is actually a parameter name.  Do not include it in
      // the list of globals, thus allowing it be of local scope.
      globals.splice(i, 1);
    }
  }
  globals = globals.length ? '  global ' + globals.join(', ') + '\n' : '';
  var funcName = Blockly.Python.variableDB_.getName(block.getFieldValue('NAME'),
      Blockly.Procedures.NAME_TYPE);
  var branch = Blockly.Python.statementToCode(block, 'STACK');
  if (Blockly.Python.STATEMENT_PREFIX) {
    branch = Blockly.Python.prefixLines(
        Blockly.Python.STATEMENT_PREFIX.replace(/%1/g,
        '\'' + block.id + '\''), Blockly.Python.INDENT) + branch;
  }
  if (Blockly.Python.INFINITE_LOOP_TRAP) {
    branch = Blockly.Python.INFINITE_LOOP_TRAP.replace(/%1/g,
        '"' + block.id + '"') + branch;
  }
  var returnValue = Blockly.Python.valueToCode(block, 'RETURN',
      Blockly.Python.ORDER_NONE) || '';
  if (returnValue) {
    returnValue = '  return ' + returnValue + '\n';
  } else if (!branch) {
    branch = Blockly.Python.PASS;
  }
  var args = [];
  for (var x = 0; x < block.arguments_.length; x++) {
    args[x] = Blockly.Python.variableDB_.getName(block.arguments_[x],
        Blockly.Variables.NAME_TYPE);
  }
  var header= '  """ this is it """\n';
  var code = 'def ' + funcName + '(' + args.join(', ') + '):\n' +
      header + globals + branch + returnValue;
  code = Blockly.Python.scrub_(block, code);
  Blockly.Python.definitions_[funcName] = code;
  return null;
};


// Blockly.Python['restop_session'] = function(block) {
//   var value_members = Blockly.Python.valueToCode(block, 'members', Blockly.Python.ORDER_ATOMIC);
//   var statements_operations = Blockly.Python.statementToCode(block, 'operations');
//   // TODO: Assemble Python into code variable.
//   var code = 'open_session(members=' + value_members +'\n';
//   return code;
// };

Blockly.Python['restop_session'] = function(block) {
  // Container for unit tests.
  //var value_members = Blockly.Python.valueToCode(block, 'members', Blockly.Python.ORDER_ATOMIC);
  var value_members = Blockly.Python.valueToCode(block, 'MEMBERS', Blockly.Python.ORDER_ATOMIC);
  //var value_members = block.getFieldValue('MEMBERS');
  //Blockly.Python.
  var resultsVar = Blockly.Python.variableDB_.getName('restopResults', Blockly.Variables.NAME_TYPE);
  // Setup global to hold test results.
  var code = resultsVar + ' = []\n';
  code= code + 'try:\n';
  code = code + '  with Session(members=' + value_members +'):\n';
  // Run tests (indented).
  code += Blockly.Python.statementToCode(block, 'DO')
      .replace(/^  /, '    ').replace(/\n  /g, '\n    ');
  code= code + 'except Exception,e:\n';
  code= code + '  exit(e)\n';
  // Destroy results.
  code += resultsVar + ' = None\n';
  // Print the report.
  return code;
};


Blockly.Python['result_should_be'] = function(block) {
  var value_result = Blockly.Python.valueToCode(block, 'result', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = 'result_should_be(' + value_result +')\n';
  return code;
};

Blockly.Python['result_value_for_key_should_be'] = function(block) {
  var value_key = Blockly.Python.valueToCode(block, 'KEY', Blockly.Python.ORDER_ATOMIC);
  var value_value = Blockly.Python.valueToCode(block, 'VALUE', Blockly.Python.ORDER_ATOMIC);
  var code = 'result_value_for_key_should_be(' + value_key + ','+ value_value  +   ')\n';
  return code;
};

Blockly.Python['sleep'] = function(block) {
  var value_timeout = Blockly.Python.valueToCode(block, 'TIMEOUT', Blockly.Python.ORDER_ATOMIC);
  var code = 'sleep(' + value_timeout +')\n';
  return code;
};

Blockly.Python['abort'] = function(block) {
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  var code = 'abort(' + value_name +')\n';
  return code;
};

Blockly.Python['log'] = function(block) {
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  var code = 'log(' + value_name +')\n';
  return code;
};

Blockly.Python['set_dry_mode'] = function(block) {
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  var code = 'set_dry_mode(' + value_name +')\n';
  return code;
};



//Blockly.Blocks['procedures_with'] = Object.create(Blockly.Blocks['procedures_callnoreturn'] );


// Blockly.Blocks['procedures_with'] = {
//   /**
//    * Block for defining a procedure with a return value.
//    * @this Blockly.Block
//    */
//   init: function() {
//     this.setHelpUrl(Blockly.Msg.PROCEDURES_DEFRETURN_HELPURL);
//     this.setColour(Blockly.Blocks.procedures.HUE);
//     var nameField = new Blockly.FieldTextInput(
//         Blockly.Msg.PROCEDURES_DEFRETURN_PROCEDURE,
//         Blockly.Procedures.rename);
//     nameField.setSpellcheck(false);
//     this.appendDummyInput()
//         .appendField(Blockly.Msg.PROCEDURES_DEFRETURN_TITLE)
//         .appendField(nameField, 'NAME')
//         .appendField('', 'PARAMS');
//     this.appendValueInput('RETURN')
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField(Blockly.Msg.PROCEDURES_DEFRETURN_RETURN);
//     this.setMutator(new Blockly.Mutator(['procedures_mutatorarg']));
//     this.setTooltip(Blockly.Msg.PROCEDURES_DEFRETURN_TOOLTIP);
//     this.arguments_ = [];
//     this.setStatements_(true);
//     this.statementConnection_ = null;
//   },
//   setStatements_: Blockly.Blocks['procedures_defnoreturn'].setStatements_,
//   validate: Blockly.Blocks['procedures_defnoreturn'].validate,
//   updateParams_: Blockly.Blocks['procedures_defnoreturn'].updateParams_,
//   mutationToDom: Blockly.Blocks['procedures_defnoreturn'].mutationToDom,
//   domToMutation: Blockly.Blocks['procedures_defnoreturn'].domToMutation,
//   decompose: Blockly.Blocks['procedures_defnoreturn'].decompose,
//   compose: Blockly.Blocks['procedures_defnoreturn'].compose,
//   dispose: Blockly.Blocks['procedures_defnoreturn'].dispose,
//   /**
//    * Return the signature of this procedure definition.
//    * @return {!Array} Tuple containing three elements:
//    *     - the name of the defined procedure,
//    *     - a list of all its arguments,
//    *     - that it DOES have a return value.
//    * @this Blockly.Block
//    */
//   getProcedureDef: function() {
//     return [this.getFieldValue('NAME'), this.arguments_, true];
//   },
//   getVars: Blockly.Blocks['procedures_defnoreturn'].getVars,
//   renameVar: Blockly.Blocks['procedures_defnoreturn'].renameVar,
//   customContextMenu: Blockly.Blocks['procedures_defnoreturn'].customContextMenu,
//   callType_: 'procedures_callnoreturn'
// };
//
//
// Blockly.Python['procedures_with'] = function(block) {
//   // Define a procedure with a return value.
//   // First, add a 'global' statement for every variable that is assigned.
//   var globals = Blockly.Variables.allVariables(block);
//   for (var i = globals.length - 1; i >= 0; i--) {
//     var varName = globals[i];
//     if (block.arguments_.indexOf(varName) == -1) {
//       globals[i] = Blockly.Python.variableDB_.getName(varName,
//           Blockly.Variables.NAME_TYPE);
//     } else {
//       // This variable is actually a parameter name.  Do not include it in
//       // the list of globals, thus allowing it be of local scope.
//       globals.splice(i, 1);
//     }
//   }
//   globals = globals.length ? '  global ' + globals.join(', ') + '\n' : '';
//   var funcName = Blockly.Python.variableDB_.getName(block.getFieldValue('NAME'),
//       Blockly.Procedures.NAME_TYPE);
//   var branch = Blockly.Python.statementToCode(block, 'STACK');
//   if (Blockly.Python.STATEMENT_PREFIX) {
//     branch = Blockly.Python.prefixLines(
//         Blockly.Python.STATEMENT_PREFIX.replace(/%1/g,
//         '\'' + block.id + '\''), Blockly.Python.INDENT) + branch;
//   }
//   if (Blockly.Python.INFINITE_LOOP_TRAP) {
//     branch = Blockly.Python.INFINITE_LOOP_TRAP.replace(/%1/g,
//         '"' + block.id + '"') + branch;
//   }
//   var returnValue = Blockly.Python.valueToCode(block, 'RETURN',
//       Blockly.Python.ORDER_NONE) || '';
//   if (returnValue) {
//     returnValue = '  return ' + returnValue + '\n';
//   } else if (!branch) {
//     branch = Blockly.Python.PASS;
//   }
//   var args = [];
//   for (var x = 0; x < block.arguments_.length; x++) {
//     args[x] = Blockly.Python.variableDB_.getName(block.arguments_[x],
//         Blockly.Variables.NAME_TYPE);
//   }
//   var code = 'with ' + funcName + '(' + args.join(', ') + '):\n' +
//       globals + branch + returnValue;
//   code = Blockly.Python.scrub_(block, code);
//   Blockly.Python.definitions_[funcName] = code;
//   return null;
// };
