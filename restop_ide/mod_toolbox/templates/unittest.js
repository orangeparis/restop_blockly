/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Unit test blocks for Blockly.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

Blockly.Blocks['unittest_main'] = {
  // Container for unit tests.
  init: function() {
    this.setColour(65);
    this.appendDummyInput()
        .appendField('run tests');
    this.appendStatementInput('DO');
    this.setTooltip('Executes the enclosed unit tests,\n' +
                    'then prints a summary.');
  },
  getVars: function() {
    return ['unittestResults'];
  }
};

Blockly.Blocks['unittest_assertequals'] = {
  // Asserts that a value equals another value.
  init: function() {
    this.setColour(65);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput('test name'), 'MESSAGE');
    this.appendValueInput('ACTUAL', null)
        .appendField('actual');
    this.appendValueInput('EXPECTED', null)
        .appendField('expected');
    this.setTooltip('Tests that "actual == expected".');
  },
  getVars: function() {
    return ['unittestResults'];
  }
};

Blockly.Blocks['unittest_assertvalue'] = {
  // Asserts that a value is true, false, or null.
  init: function() {
    this.setColour(65);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput('test name'), 'MESSAGE');
    this.appendValueInput('ACTUAL', Boolean)
        .appendField('assert')
        .appendField(new Blockly.FieldDropdown(
        [['true', 'TRUE'], ['false', 'FALSE'], ['null', 'NULL']]), 'EXPECTED');
    this.setTooltip('Tests that the value is true, false, or null.');
  },
  getVars: function() {
    return ['unittestResults'];
  }
};

Blockly.Blocks['unittest_fail'] = {
  // Always assert an error.
  init: function() {
    this.setColour(65);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput('test name'), 'MESSAGE')
        .appendField('fail');
    this.setTooltip('Records an error.');
  },
  getVars: function() {
    return ['unittestResults'];
  }
};

// Blockly.Blocks['unittest_restop'] = {
//   // Container for unit tests.
//   init: function() {
//     this.setColour(65);
//     this.appendDummyInput()
//         .appendField('restop tests');
//     this.appendStatementInput('DO');
//     this.setTooltip('Executes the enclosed restop tests,\n' +
//                     'then prints a summary.');
//   },
//   getVars: function() {
//     return ['unittestResults'];
//   }
// };
//
// Blockly.Blocks['restop_alias'] = {
//   init: function() {
//     this.appendDummyInput()
//         .appendField(new Blockly.FieldDropdown([["tv", "tv"], ["livebox", "livebox"]]), "alias");
//     this.setOutput(true, "String");
//     this.setColour(120);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };
//
//
// Blockly.Blocks['open_session'] = {
//   init: function() {
//     this.appendDummyInput()
//         .appendField("open_session");
//     this.appendValueInput("members")
//         .setCheck("String")
//         .appendField("members");
//     this.appendStatementInput("operations")
//         .appendField("operations");
//     this.setInputsInline(false);
//     this.setNextStatement(true);
//     this.setColour(20);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };
//
// Blockly.Blocks['restop_unit'] = {
//   init: function() {
//     this.appendDummyInput()
//         .appendField("unit");
//     this.appendValueInput("unit_name")
//         .setCheck("String")
//         .appendField("unit_name");
//     this.appendValueInput("members")
//         .setCheck("String")
//         .appendField("members");
//     this.setInputsInline(false);
//     this.setNextStatement(true);
//     this.setColour(20);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };
//
//
// Blockly.Blocks['controls_with'] = {
//   /**
//    * Block for repeat n times (internal number).
//    * The 'controls_repeat_ext' block is preferred as it is more flexible.
//    * @this Blockly.Block
//    */
//   init: function() {
//     this.jsonInit({
//       "message0": "with %1 block",
//       "args0": [
//         {
//           "type": "field_input",
//           "name": "TIMES",
//           "text": "10"
//         }
//       ],
//       "previousStatement": null,
//       "nextStatement": null,
//       "colour": Blockly.Blocks.loops.HUE,
//       "tooltip": Blockly.Msg.CONTROLS_REPEAT_TOOLTIP,
//       "helpUrl": Blockly.Msg.CONTROLS_REPEAT_HELPURL
//     });
//     this.appendStatementInput('DO')
//         .appendField(Blockly.Msg.CONTROLS_REPEAT_INPUT_DO);
//     this.getField('TIMES').setChangeHandler(
//         Blockly.FieldTextInput.nonnegativeIntegerValidator);
//   }
// };
//
// Blockly.Blocks['restop_session'] = {
//   init: function() {
//     this.appendDummyInput()
//         .appendField("session");
//     this.appendValueInput("MEMBERS")
//         .appendField("members");
//     this.appendStatementInput("DO");
//     this.setPreviousStatement(true);
//     this.setNextStatement(true);
//     this.setColour(120);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };