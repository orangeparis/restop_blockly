/**
 * @fileoverview REstop blocks for Blockly.
 * @author tordjman.laurent@orange.com
 */
'use strict';


Blockly.Blocks['unittest_restop'] = {
  // Container for unit tests.
  init: function() {
    this.setColour(65);
    this.appendDummyInput()
        .appendField('restop tests');
    this.appendStatementInput('DO');
    this.setTooltip('Executes the enclosed restop tests,\n' +
                    'then prints a summary.');
  },
  getVars: function() {
    return ['unittestResults','result'];
  }
};

Blockly.Blocks['restop_result'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("result");
    this.setOutput(true);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};



Blockly.Blocks['restop_alias'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["tv", "tv"], ["livebox", "livebox"]]), "alias");
    this.setOutput(true, "String");
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};


Blockly.Blocks['open_session'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("open_session");
    this.appendValueInput("members")
        .setCheck("String")
        .appendField("members");
    this.appendStatementInput("operations")
        .appendField("operations");
    this.setInputsInline(false);
    this.setNextStatement(true);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

// Blockly.Blocks['restop_unit'] = {
//   init: function() {
//     this.appendDummyInput()
//         .appendField("unit");
//     this.appendValueInput("unit_name")
//         .setCheck("String")
//         .appendField("unit_name");
//     this.appendValueInput("members")
//         .setCheck("String")
//         .appendField("members");
//     this.setInputsInline(false);
//     this.setNextStatement(true);
//     this.setColour(20);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };


Blockly.Blocks['controls_with'] = {
  /**
   * Block for repeat n times (internal number).
   * The 'controls_repeat_ext' block is preferred as it is more flexible.
   * @this Blockly.Block
   */
  init: function() {
    this.jsonInit({
      "message0": "with %1 block",
      "args0": [
        {
          "type": "field_input",
          "name": "TIMES",
          "text": "10"
        }
      ],
      "previousStatement": null,
      "nextStatement": null,
      "colour": Blockly.Blocks.loops.HUE,
      "tooltip": Blockly.Msg.CONTROLS_REPEAT_TOOLTIP,
      "helpUrl": Blockly.Msg.CONTROLS_REPEAT_HELPURL
    });
    this.appendStatementInput('DO')
        .appendField(Blockly.Msg.CONTROLS_REPEAT_INPUT_DO);
    this.getField('TIMES').setChangeHandler(
        Blockly.FieldTextInput.nonnegativeIntegerValidator);
  }
};

Blockly.Blocks['restop_session'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("session");
    this.appendValueInput("MEMBERS")
        .appendField("members");
    this.appendStatementInput("DO");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(65);
    //this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['restop_procedures'] = {
  /**
   * Block for defining a restop procedure with no return value.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.PROCEDURES_DEFNORETURN_HELPURL);
    this.setColour(Blockly.Blocks.procedures.HUE);
    var nameField = new Blockly.FieldTextInput(
        Blockly.Msg.PROCEDURES_DEFNORETURN_PROCEDURE,
        Blockly.Procedures.rename);
    nameField.setSpellcheck(false);
    this.appendDummyInput()
        .appendField(Blockly.Msg.PROCEDURES_DEFNORETURN_TITLE)
        .appendField(nameField, 'NAME')
        .appendField('', 'PARAMS');
    this.setMutator(new Blockly.Mutator(['procedures_mutatorarg']));
    //this.setTooltip(Blockly.Msg.PROCEDURES_DEFNORETURN_TOOLTIP);
    this.setTooltip('creates a restop unit');
    this.arguments_ = [];
    this.setStatements_(true);
    this.statementConnection_ = null;
  },
  setStatements_: Blockly.Blocks['procedures_defnoreturn'].setStatements_,
  validate: Blockly.Blocks['procedures_defnoreturn'].validate,
  updateParams_: Blockly.Blocks['procedures_defnoreturn'].updateParams_,
  mutationToDom: Blockly.Blocks['procedures_defnoreturn'].mutationToDom,
  domToMutation: Blockly.Blocks['procedures_defnoreturn'].domToMutation,
  decompose: Blockly.Blocks['procedures_defnoreturn'].decompose,
  compose: Blockly.Blocks['procedures_defnoreturn'].compose,
  dispose: Blockly.Blocks['procedures_defnoreturn'].dispose,
  /**
   * Return the signature of this procedure definition.
   * @return {!Array} Tuple containing three elements:
   *     - the name of the defined procedure,
   *     - a list of all its arguments,
   *     - that it DOES have a return value.
   * @this Blockly.Block
   */
  getProcedureDef: function() {
    return [this.getFieldValue('NAME'), this.arguments_, false];
  },
  getVars: Blockly.Blocks['procedures_defnoreturn'].getVars,
  renameVar: Blockly.Blocks['procedures_defnoreturn'].renameVar,
  customContextMenu: Blockly.Blocks['procedures_defnoreturn'].customContextMenu,
  callType_: 'procedures_callnoreturn'
};

Blockly.Blocks['result_should_be'] = {
  init: function () {
    this.appendValueInput("result")
        .appendField("result should be");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['result_value_for_key_should_be'] = {
  init: function() {
    this.appendValueInput("KEY")
        .appendField("result value for key");
    this.appendValueInput("VALUE")
        .appendField("should be");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};




Blockly.Blocks['sleep'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("sleep");
    this.appendValueInput("TIMEOUT")
        .setCheck("Number");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['abort'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("abort message=");
    this.appendValueInput("NAME")
        .setCheck("String");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['log'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("log message=");
    this.appendValueInput("NAME")
        .setCheck("String");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['set_dry_mode'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Boolean")
        .appendField("set dry mode");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};