# Run a test server.
import os
from app import app

from wbackend.model import Model,Database

from backend import Backend
from workers import Workers


# Configurations
#app.config.from_object('config')


redis_host= app.config.get('REDIS_HOST','localhost')
redis_port= app.config.get('REDIS_PORT', 6379)
redis_db=   app.config.get('REDIS_DB', 0)

# set environment variable for API
os.environ['REDIS_HOST']= str(redis_host)
os.environ['REDIS_PORT']= str(redis_port)
os.environ['REDIS_DB']= str(redis_db)

blockly_host= app.config.get('BLOCKLY_APP_HOST','0.0.0.0')
blockly_port= app.config.get('BLOCKLY_APP_PORT', 5016)
blockly_debug= app.config.get('BLOCKLY_APP_DEBUG', False)

os.environ['RESTOP_MASTER_URL']= app.config.get('RESTOP_MASTER_URL',"http://localhost:5000/restop/api/v1")


db = Database(host=redis_host, port=redis_port, db=redis_db)
Model.bind(database=db, namespace=None)
app.config['db'] = db
app.config['Model'] = Model


backend= Backend(app.config,redis_host=redis_host,redis_port=redis_port)
app.config['backend']=backend


# start worker manager
workers = Workers(redis_database= db,name='default')
workers.start()


# start blocky gui
app.run(host= blockly_host, port=blockly_port, debug=blockly_debug)

