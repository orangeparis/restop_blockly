import os
import re
import time
import walrus

from restop_api.transformer import get_runnable

from models import Script,TaskManager,Task

base_dir= template_dir=os.path.join(os.path.dirname(__file__))


class Backend(object):
    """

    """
    _canvas_key= "scripts:%s:canvas"
    _code_key=   "scripts:%s:code"
    _runnable_key= "scripts:%s:runnable"

    _tasklog_key= "tasks:html:id:%s"

    def __init__(self,config,*args,**kwargs):
        """

        """
        self._config= config
        self._store_directory= os.path.join(base_dir,'mod_toolbox','templates')
        self._store=Store(**kwargs)

        return


    def load_canvas(self,name=None):
        """


        :param data:
        :return:
        """
        name= name or 'default'
        filename = os.path.join(self._store_directory, "canvas", "%s.xml" % name)
        with open(filename, 'rb') as fh:
            xml_data = fh.read()
            response = xml_data
            return response


    def save_canvas(self,data,name=None):
        """
            create script

        :param name:
        :return:
        """

        record= StoreRecord(data)
        canvas= record.canvas()
        code= record.code()
        name = name or 'default'
        filename = os.path.join(self._store_directory, "canvas", "%s.xml" % name)
        with open(filename, 'wb') as fh:
            fh.write(canvas)

        # store canvas and code in db
        #self._store._db.set(self._canvas_key % name, canvas)
        #self._store._db.set(self._code_key % name, code)

        # create script model
        script= Script.create(name=name,canvas=canvas,code=code,
                              runnable='',status='updated',errors='')

        # # transform code to runnable python
        # try:
        #     runnable_code= get_runnable(code)
        # except Exception ,e:
        #     # store error
        #     script.runnable=''
        #     script.errors= str(e)
        #     script.save()
        #     return False
        #
        # # store runnable key
        # script.runnable= runnable_code
        # script.error =''
        # script.save()

        return True

    def run_canvas(self,name,data=None):
        """

            push a task

                get raw blockly code with name
                perform  transformation
                push a task

        :param name:
        :param data:
        :return:
        """
        # load script
        script= Script.load(name)

        # create new task
        task = script.create_task()
        task_id = task.id

        raw_code= script.code

        # get raw blockly python code
        #raw_code= self._store._db.get(self._code_key % name)

        # transform code to runnable python
        try:
            code= get_runnable(raw_code,run_id=task_id)
            task.logs.append("compute runnable code")
        except Exception ,e:
            # store error
            task.status= 'aborted'
            task.save()
            task.logs.append('cannot compute runnable: %s' % repr(e))
            script.runnable=''
            script.errors= repr(e)
            script.save()
            return -1

        # store runnable key
        script.runnable= code
        script.error =''
        script.save()

        task.code= code
        task.save()

        # enqueue task
        manager= TaskManager.get_or_create()
        manager.enqueue_task(task)


        # store runnable key
        #self._store._db.set(self._runnable_key % name, code)

        # # enqueue task
        # timestamp = time.time()
        # runner= Runner(self._store._db)
        # runner.push_task(name,code=code,timestamp=timestamp)
        # task_id= str(timestamp)




        return task_id


    def get_tasklog(self,task_id):
        """

        :param task_id:
        :return:
        """
        # tasks:html:id:%s
        task= Task.load(task_id)
        #htmllog= self._store._db.get(self._tasklog_key % task_id)
        htmllog= task.html
        return htmllog




class Store(object):
    """


    """
    prefix= "blockly"


    def __init__(self,redis_host='localhost',redis_port=6379,**kwargs):
        """

        :param kwargs:
        """
        self.prefix= kwargs.get('redis_prefix',self.prefix)
        self._db= walrus.Database(host=redis_host,port=redis_port)
        self._kwargs= kwargs



class StoreRecord(object):
    """

        handle the format of the /toolbox/save request

            file=<!DOCTYPE python-export>
            <network><keywords flag="true"/>
              <node nodeId="1" name="restop">
                 ['test name', {{ XML_CANVAS }} , {{ PYTHON_CODE }} ],
              </node>
            </network>


        see tests/samples/save_sample


    """
    _node_info=re.compile('\<node nodeId=(.*?) name=(.*?)\>')
    _xml_part= re.compile("('<xml .*</xml>')",re.DOTALL)
    _code_part= re.compile("'</xml>',(.*)\],</node>",re.DOTALL)

    def __init__(self,data):
        """


        :param data:
        """
        self._data= data

        self.node_type= None
        self.node_id= None
        self.node_name= None

        self._parse()

    def _parse(self,data=None):
        """

        :param data:
        :return:
        """
        data= data or self._data

        # assert data type validity  file=<!DOCTYPE python-export>
        assert data.startswith('file=<!DOCTYPE python-export>')
        self.node_type= 'python-export'

        # extract node info
        match= self._node_info.search(data)
        if match:
            self.node_id= match.group(1).strip('"')
            self.node_name= match.group(2).strip('"')

        return

    def canvas(self):
        """

        :return:
        """
        match= self._xml_part.search(self._data)
        if match:
            xml= match.group(1)
            result=[]
            parts=[]
            #if '+' in xml:
            parts = xml.split('\n')
            for part in parts:
                #text = part.strip().rstrip('+').strip().strip("'").rstrip().strip('\\n')
                text = part.strip().rstrip('+').strip().strip("'").rstrip()
                if text.endswith('\\n'):
                    text= text[:-2]
                if text:
                    result.append(text)
            return "\n".join(result)
        else:
            return ''

    def code(self):
        """

        :return:
        """
        match= self._code_part.search(self._data)
        if match:
            code= match.group(1)
            result=[]
            parts = code.split('\n')
            for part in parts:
                # sample: 'unittestResults = None\n' +
                #if "variance" in part:
                #    pass
                #text = part.strip().rstrip('+').strip().strip("'").rstrip().strip('\\n')
                text = part.strip().rstrip('+').strip().strip("'").rstrip()
                if text.endswith('\\n'):
                    text=text[:-2]

                #if text:
                result.append(text)
            return "\n".join(result)
        else:
            return ''
