import os
import flask
from flask import Blueprint, render_template, abort,request,current_app


mod_doc = Blueprint('doc', __name__,template_folder='templates')

#template_dir=os.path.join(os.path.dirname(__file__),"templates")




@mod_doc.route('/doc/',methods=['GET'])
def docindex():
    """

    :return:
    """
    backend = current_app.config['backend']

    return render_template('docindex.html')
