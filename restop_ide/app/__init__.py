# Import flask and template operators
import flask
from flask import Flask, render_template,redirect
from flask_bootstrap3 import Bootstrap
from flask_navigation import Navigation

from restop_ide.mod_toolbox import toolbox
from restop_ide.mod_tasks import mod_tasks
from restop_ide.mod_doc import mod_doc


# Impreort SQLAlchemy
#from flask.ext.sqlalchemy import SQLAlchemy

# Define the WSGI application object
app = Flask(__name__)

# bootstrap
Bootstrap(app)

# Configurations
app.config.from_object('config')


# register blueprints
app.register_blueprint(toolbox,url_prefix='/ide')
app.register_blueprint(mod_tasks,url_prefix='/ide')
app.register_blueprint(mod_doc,url_prefix='/ide')

# app.register_blueprint(toolbox)
# app.register_blueprint(mod_tasks)


# register flask-navigation
nav = Navigation(app)

# create navbar
nav.Bar('top', [
    nav.Item('Home', 'index'),
    nav.Item('Toolbox', 'toolbox.root' ),
    nav.Item('Tasks', 'tasks.tasks'),
    nav.Item('Doc','doc.docindex'),
    nav.Item('Grafana','grafana'),
    nav.Item('Redis','redis_commander')
    #nav.Item('dummies', 'dummy.dummy_list'),
])



# Define the database object which is imported
# by modules and controllers
#db = SQLAlchemy(app)
db=None



@app.route('/ide')
#@app.route('/')
#@app.route('/index')
def index():
    """

    :return:
    """
    return flask.render_template('index.html')



@app.route('/ide/grafana')
def grafana():
    """

    :return:
    """
    host= app.config.get('GRAFANA_HOST','grafana')
    port= app.config.get('GRAFANA_PORT',3000)
    #grafana_host= '192.168.99.100'
    return redirect('http://%s:%d' % (host,port))


@app.route('/ide/redis')
def redis_commander():
    """

    :return:
    """
    station_ip= app.config['RESTOP_STATION_IP']
    host = app.config.get('REDIS_COMMANDER_HOST', station_ip)
    port= app.config.get('REDIS_COMMANDER_PORT',8081)
    # host= '192.168.99.100'
    return redirect('http://%s:%d' % (host,port))





        # Sample HTTP error handling
# @app.errorhandler(404)
# def not_found(error):
#     return render_template('404.html'), 404

# Import a module / component using its blueprint handler variable (mod_auth)
#from app.mod_auth.controllers import mod_auth as auth_module

# Register blueprint(s)
#app.register_blueprint(auth_module)
# app.register_blueprint(xyz_module)
# ..

# Build the database:
# This will create the database file using SQLAlchemy
#db.create_all()