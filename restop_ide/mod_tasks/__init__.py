import os
import json
import flask
from flask import Blueprint, render_template, abort,request,current_app
from jinja2 import TemplateNotFound
#from forms import TasksForm
from restop_ide.models import Task

mod_tasks = Blueprint('tasks', __name__,template_folder='templates')

template_dir=os.path.join(os.path.dirname(__file__),"templates")

# @tasks.route('/tasks')
# def root():
#         return "/tasks"



@mod_tasks.route('/tasks/log/<task_id>',methods=['GET'])
def tasklogs(task_id):
    """

    :return:
    """
    backend = current_app.config['backend']
    task= Task.load(task_id)
    if not task:
        abort(404)
    if task.status == 'done':
        htmllog= backend.get_tasklog(task_id)
        return htmllog
    elif task.status== 'aborted':
        return "task aborted:\n"
    else:
        # still running
        return "task still running\n"

@mod_tasks.route('/tasks', methods=['GET'])
def tasks():
    """
        list tasks
    :return:
    """
    backend = flask.current_app.config['backend']

    tasks = Task.iter_task(user_id='default')

    return flask.render_template('tasks.html', tasks=tasks)





#
#
#
# @mod_tasks.route('/task', methods=['GET', 'POST'])
# def tasks_form():
#     """
#         create a task
#     :return:
#     """
#     backend = flask.current_app.config['backend']
#
#     form = TasksForm()
#
#     user = 'current_user'
#
#     whitelist = []  # user targets whitelist
#     # get flask session and whitelist from the flask session
#     session = flask.globals.session
#     whitelist = session['whitelist']
#
#     # POST request
#     if form.validate_on_submit():
#
#         user = 'current_user'
#         user_id = '1'
#
#         target_type = form.target_type.data
#         targets = form.targets.data
#         begin_period = form.begin_period.data
#         end_period = form.end_period.data
#         description = form.description.data
#
#         tasksform_targets_list = targets.split(",")  # get the list of targets (comma separated)
#
#         # Check if all targets are in the whitelist (remove white spaces in the form targets)
#         for target in tasksform_targets_list:
#             clean_target = re.sub(r'\s+', '', target)
#             if clean_target not in whitelist:
#                 flask.flash('Your profile does not allow to use target {}'.format(clean_target), 'error')
#                 return flask.redirect(flask.url_for("tasks.tasks_form"))
#
#         data = {
#             'session_id': None, 'user_id': user_id,
#             'parameters': {
#                 'target_type': target_type,
#                 'targets': targets,
#                 'begin_period': str(begin_period),
#                 'end_period': str(end_period)
#             },
#             'description': description,
#         }
#
#         task = Task.auto_new(data, backend)
#
#
#         flask.flash('Task submitted: %s ' % task._id)
#
#         return flask.redirect(flask.url_for('tasks.tasks'))
#
#     # GET request
#     return flask.render_template('tasks_form.html', user=user, form=form, header="TASKS launcher")
#
#





