import os
import binascii
from datetime import timedelta

from walrus import *
from wbackend.model import Model
from wbackend.graph import Graph

import logging
log= logging.getLogger('platform')


class Script(Model):
    """


    """
    collection= 'script'

    name= TextField(primary_key=True)

    status = TextField(default='created')
    errors = TextField(default='')

    canvas= TextField()
    code= TextField()
    runnable= TextField()



    def get_tasks(self):
        """
            get a list of tasks objects for this script


        :return:
        """
        graph = Graph(self.database, namespace='links')
        X = graph.v.X  # Create a variable placeholder.
        results = graph.search({'s': self.get_hash_id(), 'p': 'HAS_TASK', 'o': X})
        tasks=[]
        for task_key in results['X']:
            task= Task.load(primary_key=task_key,convert_key=False)
            tasks.append(task)
        return tasks


    def create_task(self,task_id= None, **kwargs):
        """

        :param task_id:
        :return:
        """
        task_id= task_id or int(time.time())
        script= self.name
        try:
            code = self.runnable
        except AttributeError:
            code= self.code
        # create the task
        task= Task.create(task_id,code=code,script=script,**kwargs)
        # setup links
        graph= Graph(self.database,namespace='links')
        # add links enterprise HAS site
        graph.store(self.get_hash_id(), 'HAS_TASK', task.get_hash_id())

        return task

class Task(Model):
    """
            a blockly task

    """
    collection= 'task'

    id = TextField(primary_key=True)
    script = TextField(index=True,default='script')
    created = DateField(index=True, default=datetime.datetime.utcnow())

    status= TextField(index=True,default='created')

    owner= TextField(index=True,default='default')

    # the python source code to run
    code= TextField()

    logs = ListField()

    # yaml trace
    trace= ListField()

    display= ListField()

    # robot framework output.xml
    xml= TextField()
    # robot framework log.html
    html= TextField()

    data = JSONField()

    @classmethod
    def create(cls, task_id,**kwargs):
        """
        Create a new token
        """
        # members=[]
        # if 'members' in kwargs:
        #     members= kwargs.pop('members')
        id = str(task_id)
        instance = cls(id=id,**kwargs)
        instance.save()
        instance.logs.append('task model created')
        #instance.setup(members=members)
        return instance

    def get_script(self):
        return Script.get(Script.name == self.script)


    @classmethod
    def iter_task(cls,user_id=None):
        """
            iter all task

        """
        for task in Task.query(order_by=Task.id):
        #for task in Task.all():
            yield task

            # if not user_id:
            #     yield task
            # else:
            #     if task.data.get('user_id',None) == str(user_id):
            #          yield task

    def get_display_status(self):
        """
        :return: a bootstrap status ( active/success/warning/danger
        """
        if self.status == 'done':
            return 'success'
        elif self.status == 'aborted':
            return 'danger'
        else:
            return self.status

        # if status is None:
        #     return "warning"
        # elif str(status) is "0":
        #     return "success"
        # elif status in ["127","2"]:
        #     return "danger"
        # else:
        #     return "active"
        #

class TaskManager(Model):
    """

    """
    collection= 'task_manager'


    name= TextField(primary_key=True,default='default')

    queue= ListField()
    logs= ListField()


    @classmethod
    def get_or_create(self,name='default'):
        """

        :param name:
        :return:
        """
        try:
            tm = TaskManager.load(name)
        except KeyError:
            # dont exists: create it
            tm = TaskManager.create(name=name)
            tm.logs.append('Task Manager Created')
        return tm

    def enqueue_task(self,task):
        """

        :param task: string task_id or Task Object
        :return:
        """
        if isinstance(task,Task):
            task_id = task.id
        else:
            task_id= task

        # put in queue
        self.queue.append(task_id)

        # log it
        self.logs.append("push task: %s" % task_id)

        # add link
        graph = Graph(self.database, namespace='links')
        # add links enterprise HANDLE TASK
        graph.store(self.get_hash_id(), 'HANDLE_TASK', task.get_hash_id())

        return


    # def pop_task(self):
    #     """
    #
    #     :return:
    #     """
    #     task= None
    #     if len(self.task_queue):
    #         # there is a task: pop it and return it
    #         task= json.loads(self.task_queue.popleft())
    #         self.log.debug("pop task: %s" % Task(task).display())
    #     return task

    def dequeue(self):
        """

        :return:
        """
        task = None
        if len(self.queue):
            # there is a task: pop it and return it
            task_id= self.queue.popleft()
            #task= Task.load(task_id)
            self.logs.append("pop task: %s" % task_id)
            task= Task.load(task_id)
        return task

    def get_tasks(self):
        """
            get a list of tasks objects for this script


        :return:
        """
        graph = Graph(self.database, namespace='links')
        X = graph.v.X  # Create a variable placeholder.
        results = graph.search({'s': self.get_hash_id(), 'p': 'HANDLE_TASK', 'o': X})
        tasks=[]
        for task_key in results['X']:
            task= Task.load(primary_key=task_key,convert_key=False)
            tasks.append(task)
        return tasks



