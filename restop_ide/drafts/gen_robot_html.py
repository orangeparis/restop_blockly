import os
from robot.conf import RebotSettings
from robot.utils.robotio import file_writer
from robot.reporting.logreportwriters import RobotModelWriter
from robot.reporting.resultwriter import Results
from robot.htmldata.htmlfilewriter import HtmlFileWriter


output=file_writer(os.path.realpath('../tests/samples/output.xml'))

#writer= HtmlFileWriter( output, model_writer)
options= {}
settings= RebotSettings(options)
results= Results(settings)

js_model=results.js_result

template= "log.html"

config= {
    'reportURL': 'report.html', 'splitLogBase': u'log', 'defaultLevel': 'TRACE', 'minLevel': 'INFO',
    'title': ''}


with output:
    model_writer = RobotModelWriter(output, js_model, config)
    writer = HtmlFileWriter(output, model_writer)
    writer.write(template)

print "Done"
