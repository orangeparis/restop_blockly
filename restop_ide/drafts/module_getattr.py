#
# a way for a module to have __getattr__ functionality
#
# see: http://stackoverflow.com/questions/2447353/getattr-on-a-module

# module foo.py

import sys

class Foo:

    def funct1(self, *args):
        """
        <code>
        """
    def funct2(self, *args):
        """
        <code>
        """

sys.modules[__name__] = Foo()

