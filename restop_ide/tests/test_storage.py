import difflib

from restop_ide.backend import StoreRecord


result_xml= '''\
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="unittest_main" x="37" y="17">
    <statement name="DO">
      <block type="unittest_assertvalue">
        <field name="MESSAGE">my_test</field>
        <field name="EXPECTED">TRUE</field>
        <value name="ACTUAL">
          <block type="procedures_callreturn">
            <mutation name="unit_2">
              <arg name="members"></arg>
            </mutation>
            <value name="ARG0">
              <block type="text">
                <field name="TEXT">tv,livebox</field>
              </block>
            </value>
          </block>
        </value>
        <next>
          <block type="procedures_callnoreturn">
            <mutation name="unit">
              <arg name="members"></arg>
              <arg name="timeout"></arg>
            </mutation>
            <value name="ARG0">
              <block type="text">
                <field name="TEXT">tv,livebox</field>
              </block>
            </value>
            <value name="ARG1">
              <block type="math_number">
                <field name="NUM">10</field>
              </block>
            </value>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="procedures_defnoreturn" x="59" y="238">
    <mutation>
      <arg name="tv"></arg>
    </mutation>
    <field name="NAME">unit_tv_basic</field>
    <statement name="STACK">
      <block type="restop_power_on">
        <value name="alias">
          <block type="variables_get">
            <field name="VAR">tv</field>
          </block>
        </value>
      </block>
    </statement>
  </block>
  <block type="procedures_defreturn" x="62" y="342">
    <mutation>
      <arg name="members"></arg>
    </mutation>
    <field name="NAME">unit_2</field>
    <statement name="STACK">
      <block type="restop_expect">
        <value name="alias">
          <block type="text">
            <field name="TEXT">tv</field>
          </block>
        </value>
        <value name="pattern">
          <block type="text">
            <field name="TEXT">pong</field>
          </block>
        </value>
        <value name="timeout">
          <block type="math_number">
            <field name="NUM">5</field>
          </block>
        </value>
      </block>
    </statement>
    <value name="RETURN">
      <block type="logic_boolean">
        <field name="BOOL">TRUE</field>
      </block>
    </value>
  </block>
  <block type="procedures_defnoreturn" x="63" y="489">
    <mutation>
      <arg name="members"></arg>
      <arg name="timeout"></arg>
    </mutation>
    <field name="NAME">unit</field>
    <statement name="STACK">
      <block type="restop_expect">
        <value name="alias">
          <block type="text">
            <field name="TEXT">tv</field>
          </block>
        </value>
        <value name="pattern">
          <block type="text">
            <field name="TEXT">hello</field>
          </block>
        </value>
        <value name="timeout">
          <block type="variables_get">
            <field name="VAR">timeout</field>
          </block>
        </value>
      </block>
    </statement>
  </block>
</xml>'''


result_code='''\

unittestResults = None
members = None
timeout = None
tv = None

def unittest_report():
  # Create test report.
  report = []
  summary = []
  fails = 0
  for (success, log, message) in unittestResults:
    if success:
      summary.append(".")
    else:
      summary.append("F")
      fails += 1
      report.append("")
      report.append("FAIL: " + message)
      report.append(log)
  report.insert(0, "".join(summary))
  report.append("")
  report.append("Number of tests run: %d" % len(unittestResults))
  report.append("")
  if fails:
    report.append("FAILED (failures=%d)" % fails)
  else:
    report.append("OK")
  return "\\n".join(report)

def assertEquals(actual, expected, message):
  # Asserts that a value equals another value.
  if unittestResults == None:
    raise Exception("Orphaned assert equals: " + message)
  if actual == expected:
    unittestResults.append((True, "OK", message))
  else:
    unittestResults.append((False, "Expected: %s\\nActual: %s" % (expected, actual), message))

def unit_tv_basic(tv):
  power_on( tv )

def unit_2(members):
  expect( 'tv', pattern='pong', timeout=5, cancel_on=, regex= )
  return True

def unit(members, timeout):
  expect( 'tv', pattern='hello', timeout=timeout, cancel_on=, regex= )


unittestResults = []
assertEquals(unit_2('tv,livebox'), True, 'my_test')
unit('tv,livebox', 10)
report = unittest_report()
unittestResults = None
print(report)
'''




def get_sample():
    """


    :return:
    """
    with open('./samples/save_sample','rb') as fh:
        data= fh.read()
    return data




def test_StoreRecord():
    """

    :return:
    """
    data= get_sample()
    s= StoreRecord(data)


    assert s.node_type == 'python-export'
    assert s.node_id == '1'
    assert s.node_name == 'restop'

    xml= s.canvas()
    assert xml == result_xml

    code= s.code()

    diffs= difflib.ndiff(code, result_code)

    for i,s in enumerate(diffs):
        if s[0]==' ': continue
        elif s[0]=='-':
            print(u'Delete "{}" from position {}'.format(s[-1],i))
        elif s[0]=='+':
            print(u'Add "{}" to position {}'.format(s[-1],i))
    print()


    assert code == result_code

    return




#
#
#
if __name__=="__main__":

    test_StoreRecord()
    print "Done"