import yaml
import pprint
from restop_ide.restop_api.xmlout import XmlOut



#trace_filename= 'dummy.yml'
trace_filename= './samples/trace.yml'


def check_yaml(filename):
    """

    :param filename:
    :return:
    """
    data = yaml.load(file(filename))

    pprint.pprint(data)

    return data



def test_yaml():
    """


    :return:
    """
    check_yaml(trace_filename)
    return



def test_basic():
    """

    :return:
    """
    data= check_yaml(trace_filename)

    x= XmlOut(data)

    data= dict()


    #for info in x._iter_data():
    #    print info
    #kw_text= x.get_keyword(data)

    x.gen_xml()

    xml= x.xml()

    html= x.html()

    return




if __name__=="__main__":

    #test_yaml()
    test_basic()

    print "Done"




