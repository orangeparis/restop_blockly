import pprint
import yaml
import walrus
#from restop_ide.restop_api import get_api

from restop_ide.restop_api.transformer import get_runnable, run_python_source

#data= yaml.load(file('samples/trace.yml'))
#pprint.pprint(data)

source_key = "scripts:%s:code"
runnable_key = "scripts:%s:runnable"

trace_key = "runs:%s:trace"




def redis_db():
    """

    :return:
    """
    return walrus.Database()

def raw_source(name):
    """
        get the blockly source in redis
    :param name:
    :return:
    """
    db= redis_db()
    #db.flushdb()

    source= db.get(source_key % name)

    return source

def runnable_source(name):
    """

    :param name:
    :return:
    """
    source= raw_source(name)
    runnable= get_runnable(source)

    db= redis_db()
    db.set(runnable_key % name, runnable)

    return runnable



def test_basic():
    """


    :return:
    """
    name= 'restop'
    source= runnable_source(name)
    if source:
        rc= run_python_source(source)

    db= redis_db()
    with open("trace.yml","rb") as fh:
        content= fh.read()
        db.set(trace_key % name,content)
    return


def test_math():
    """

    :return:
    """
    name= 'math'
    #source= runnable_source(name)
    source=raw_source(name)
    if source:
        rc= run_python_source(source)

    db= redis_db()
    with open("trace.yml","rb") as fh:
        content= fh.read()
        db.set(trace_key % name,content)
    return


if __name__=="__main__":


    test_basic()
    #test_math()
    print "Done"

