import json
from restop_ide.mod_toolbox.generate_restop_blocks import Methods


json_files = dict(
    tvbox= '../mod_toolbox/templates/tvbox_data.json',
    livebox= '../mod_toolbox/templates/livebox_data.json',
)


categories= ['tvbox', 'livebox']
methods = {}

try:
    for name in categories:
        filename = json_files[name]
        with open(filename, 'rb') as fh:
            json_data = fh.read()
            data = json.loads(json_data)
            methods[name] = Methods(data['item_methods'])


    for name in categories:

        for method in methods["tvbox"].iter_methods():
            type = method.type_name
            for field, value in method.iter_default_values():
                name= field
                value= method.simple_value(value)

                print name,value
except:
    raise


    print "Done"