import time
import walrus
from restop_ide.workers import Workers


def db():
    return walrus.Database()



def test_basic():

    redis_db= db()

    w = Workers()

    w.push_task('restop','print("hello world\n")')


    task= w.pop_task()
    assert task['name'] == 'restop'

    task= w.pop_task()
    assert task is None

    return

def test_run_once():
    """

    :return:
    """
    redis_db= db()

    w = Workers()

    #w.push_task('restop','print("hello world\n")')

    rc= w.run_once()

    #
    time.sleep(20)

    return




def test_threaded():
    """

    :return:
    """
    redis_db= db()

    w = Workers()

    w.push_task('restop','print("hello world\n")')
    time.sleep(1)

    w.start()

    time.sleep(5)

    return



if __name__=="__main__":
    """


    """
    #test_basic()
    test_run_once()

    #test_threaded()


    print "Done"