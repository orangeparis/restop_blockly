import ast
import threading
import time


def execute_code( code ):
    """

    :param code: string : python source code
    :return:
    """
    tree = ast.parse(code)
    exec (compile(tree, filename="<ast>", mode="exec"))


def execute_code_in_thread(code,daemon=True):
    """

    :param code: string: python source code
    :param daemon: boolean mode daemon ( if True the thread dies if the master dies)
    :return:
    """
    t= threading.Thread(target=execute_code,args=(code,))
    t.setDaemon(daemon)
    t.start()
    return t


code='''\
import time

for i in xrange(0,10):
  print "."
  time.sleep(1)
print "child Done"
'''


def test_basic():
    """

    :return:
    """

    t= execute_code_in_thread(code)
    t.join()

    return


def test_sample_blockly_code():
    """

    :return:
    """
    with open("samples/python_runtime2.py", "rb") as fh:
        code = fh.read()
        t= execute_code_in_thread(code)
        #t.join()
        time.sleep(3)

    return

if __name__=='__main__':

    from restop_ide.restop_api import get_api
    api = get_api()

    #test_basic()
    test_sample_blockly_code()

    print "master Done"