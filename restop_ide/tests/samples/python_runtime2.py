from restop_ide.restop_api import Api
run_id = '1487264060'
api = Api(run_id=run_id)
unittestResults = None
device = None
timeout = None

def unit(device, timeout):
    ' this is it '
    restopResults = []
    try:
        with api.Session(members=[device]):
            result = api.sync(device, timeout=timeout)
            result = api.expect(device, pattern='tv', timeout=5, cancel_on='', regex='no')
            result = api.result_should_be(True)
            result = api.watch(device, timeout=5)
    except Exception as e:
        pass
    restopResults = None
unittestResults = []
result = {}
with api.Suite():
    with api.Runner():
        unit('tv', 10)
    with api.Runner():
        unit('tv2', 10)