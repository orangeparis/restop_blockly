from restop_ide.restop_api import get_api
api = get_api()


def unit(members='tv,livebox', timeout=10):
    with api.Session(members):
        api.send('device',timeout=timeout)
        api.wait('device',timeout=10)
    return



print("START")

unit('tv1,lb1',timeout=5)


api._enter('while')
while 1:
    api.send('livebox', cmd='echo "hello"')
    api._enter('if')
    if True:
        print 'this is true'
    else:
        print 'do nothing'
    api._leave('if')
    break
api._leave('while')



print "Done"

