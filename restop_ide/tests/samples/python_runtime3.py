import os
os.environ['RESTOP_MASTER_URL']= "http://192.168.1.21/restop/api/v1"

from restop_ide.restop_api import Api
run_id = '1488995874'
api = Api(run_id=run_id)
device = None
channel = None
label = None
unittestResults = None
result = None
timeout = None

def change_channel(device, channel, label):
    result = api.send_key(device, key=channel, timeout=5, wait=False)
    result = api.sleep(2)
    result = api.page_get_info_from_live_banner(device)
    result = api.result_value_for_key_should_be('channelName', label)

def unit_restop(device, timeout):
    ' this is it '
    restopResults = []
    try:
        with api.Session(members=[device]):
            result = api.log('D\xc3\xa9part')
            change_channel(device, '1', 'TF1')
            result = api.sleep(3)
    except Exception as e:
        result = api.exit(e)
    restopResults = None
unittestResults = []
result = {}
with api.Suite():
    api.set_dry_mode(False)
    with api.Runner():
        unit_restop('tv', 1)