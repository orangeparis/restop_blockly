unittestResults = None
tv = None
timeout = None

def restop_report():
  # Create test report.
  report = []
  summary = []
  fails = 0
  for (success, log, message) in unittestResults:
    if success:
      summary.append(".")
    else:
      summary.append("F")
      fails += 1
      report.append("")
      report.append("FAIL: " + message)
      report.append(log)
  report.insert(0, "".join(summary))
  report.append("")
  report.append("Number of tests run: %d" % len(unittestResults))
  report.append("")
  if fails:
    report.append("FAILED (failures=%d)" % fails)
  else:
    report.append("OK")
  return "\n".join(report)

def restop_unit(tv, timeout):
  """ this is it """
  restopResults = []
  with api.open_session(members=[tv, 'livebox']):
    power_on( tv )
    serial_synch( tv, timeout=timeout )
    for count in range(10):
      op_dummy( tv, item='item' )
  restopResults = None


unittestResults = []
restop_unit('tv1', 10)
report = restop_report()
unittestResults = None
print(report)

if __name__=="__main__":
    from restop_ide.restop_api import get_api
    api= get_api()