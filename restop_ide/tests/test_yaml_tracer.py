import pprint
import yaml
import time

from restop_ide.restop_api import YamlTracer


filename="dummy.yml"


sample_message='''\
[{'ended': 1486656624,
  'keyword': 'open_something',
  'result': {'logs': 'this is\nis a long \nlog fdfdfk ehrekjekjekjekjek\ndsdkjskjkjkjkjkjkjkjkjk\nrererereeree\ndndifijifrihrhfnrn jeje\nrtrkltrklktr\n',
             'message': 'OK',
             'status': 200},
  'started': 1486656624}]
'''



def check_yaml(filename):
    """

    :param filename:
    :return:
    """
    data = yaml.load(file(filename))

    pprint.pprint(data)



def test_yaml():
    """

    :return:
    """
    check_yaml(filename)
    return


def test_basic():
    """


    :return:
    """
    y= YamlTracer(filename=filename)

    y.keyword_start("open_something")
    y.keyword_status("PASS")
    y.keyword_end()

    y.keyword_start("while",body=True)
    y.keyword_start("do_something")
    y.keyword_status("PASS")
    y.keyword_end()
    y.keyword_end()

    check_yaml(filename)
    return


def test_messages():
    """

    :return:
    """
    y = YamlTracer(filename=filename)

    message= 'this is my message'

    y.report_start()
    y.suite_start()
    y.testcase_start()
    y.keyword_start("open_something")
    time.sleep(1)
    y.keyword_msg(message)
    y.keyword_status("PASS")
    y.keyword_status(status='PASS')
    y.keyword_end()
    y.testcase_end()
    y.suite_end()
    y.report_end()

    check_yaml(filename)
    return





def test_logs():
    """

    :return:
    """
    y = YamlTracer(filename=filename)

    logs= [ 'this is', 'is ' 'a long ','log fdfdfk ehrekjekjekjekjek', 'dsdkjskjkjkjkjkjkjkjkjk','rererereeree',
            'dndifijifrihrhfnrn jeje', 'rtrkltrklktr']



    y.keyword_start("open_something")
    y.keyword_status("PASS")
    y.keyword_end()



    check_yaml(filename)
    return

if __name__ == "__main__":

    test_yaml()
    #test_basic()
    #test_logs()
    #test_messages()

    print 'Done'