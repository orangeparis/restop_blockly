from wbackend.model import Database,Model

from restop_ide.models import Script, Task

def redis_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    #db.flushdb()
    Model.bind(database=db,namespace=None)
    return db


def test_basic():

    db= redis_db()

    #script=Script.create_or_update()

    script= Script.create(name='restop',code='print("hello world\n")')

    script.runnable='print("hello world\n")'
    script.save()

    print script

    t1= script.create_task()

    print t1.id
    t1.logs.append('start working')
    t1.trace.append('# start task \n\n')
    t1.display.append('TEST\n###########################\n')



    # t= Task.create('1487266621')
    #
    #
    # t.logs.append('start working')
    # t.trace.append('# start task \n\n')
    # t.display.append('TEST\n###########################\n')

    print t1


    return


if __name__== "__main__":


    test_basic()
    print "Done"
