"""
    workers.py a task manager


    a loop
        scan task queue blockly:q:main
        when a task is present pop it and spawn a worker

        log actions in blockly:tasks:container:tasks:log

"""

import time
import threading
import walrus


#from tools.restop_adapters.logger import SimpleLogger as Logger
from wbackend.logger import SimpleLogger as Logger

from models import Database, TaskManager
from models import Task

from restop_adapters.adapter_python import PythonThreadedAdapter

from restop_adapters import dashboard_keep_alive, dashboard_pid, dashboard_status
from restop_api.xmlout import XmlOut


from config import REDIS_HOST,REDIS_PORT,REDIS_DB

redis_host= REDIS_HOST
redis_port= REDIS_PORT
redis_db= REDIS_DB



class Adapter(PythonThreadedAdapter):
    """
        adapter to launch a blockly python source in a thread

    """


class Workers(threading.Thread):
    """
        worker manager

    """
    def __init__(self,redis_database= None , name='default'):
        """

        :param kwargd:
        """
        threading.Thread.__init__(self)
        self.db = redis_database or Database(host=redis_host, port=redis_port, db=redis_db)

        self._task_manager = TaskManager.get_or_create(name=name)
        self.agent_id= self._task_manager.get_hash_id()
        self.log = Logger("taskmanager:container:logs:%s" % self.agent_id, redis_db=self.db)

        self._keep_alive= walrus.Hash(self.db,dashboard_keep_alive)
        self._status= walrus.Hash(self.db, dashboard_status)
        self._pid= walrus.Hash(self.db, dashboard_pid)



        self.Terminated= False
        self.setDaemon(True)

    def keep_alive(self,clear=False):
        """

            write time to /agents[agent_id]
        :return:
        """
        if not clear:
            self._keep_alive[self.agent_id] = time.time()
            self._status[self.agent_id] = 'running'
        else:
            # the child thread is not running
            self._status[self.agent_id] = 'not running'
            self._keep_alive[self.agent_id] = None
        return

    def run_once(self):
        """

        :return:
        """
        self.keep_alive()

        #task= self.pop_task()
        task= self._task_manager.dequeue()
        if task is not None:
            # launch a work on the task
            self.spawn(task)
            return True
        else:
            return False


    def run(self):
        """


        :return:
        """

        while not self.Terminated:

            rc= self.run_once()
            if rc:
                # a task has been launched
                pass
            # pause
            time.sleep(1)
        # leave
        return


    def spawn(self,task):
        """

        :param task:
        :return:
        """
        code= task.code
        task_key= task.get_hash_id()
        self.log.debug("spawn a worker for task: %s " % task.id)

        adapter= Adapter(task_key,command_line=code,redis_db=self.db)
        adapter.start()

        # on this version workers runs only one worker a a time
        # wait for the worker to finish
        self._status[self.agent_id] = 'wait worker'
        adapter.join()
        self._status[self.agent_id] = 'finish worker'
        self.worker_finished(task.get_id())

        return


    def worker_finished(self,task_id):
        """

        :param task_id:
        :return:
        """
        worker_log= walrus.List(self.db,"task:container:trace:task:id:%s" % task_id)
        lng= len(worker_log)
        if lng:

            task= Task.load(task_id)



            # handle trace
            lines= "".join(worker_log)
            x = XmlOut.from_string(lines)
            x.gen_xml()
            xml = x.xml()

            task.xml=xml
            #self.db.set("tasks:xml:id:%s" % task_id, xml)


            html = x.html()
            task.html= html
            #self.db.set("tasks:html:id:%s" % task_id, html)

            task.status= 'done'
            task.save()
            pass
        else:
            # no log
            pass

        return


    def enqueue_task(self,task):
        """

        :param task:
        :return:
        """
        return self._task_manager.enqueue_task(task=task)

    def dequeue(self):
        """

        :return:
        """
        return self._task_manager.dequeue()



if __name__=="__main__":

    workers= Workers()
    workers.start()

    print "exit workers"

