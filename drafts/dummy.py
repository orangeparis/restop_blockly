unittestResults = None
ok = None

def unittest_report():
  # Create test report.
  report = []
  summary = []
  fails = 0
  for (success, log, message) in unittestResults:
    if success:
      summary.append(".")
    else:
      summary.append("F")
      fails += 1
      report.append("")
      report.append("FAIL: " + message)
      report.append(log)
  report.insert(0, "".join(summary))
  report.append("")
  report.append("Number of tests run: %d" % len(unittestResults))
  report.append("")
  if fails:
    report.append("FAILED (failures=%d)" % fails)
  else:
    report.append("OK")
  return "\n".join(report)

def assertEquals(actual, expected, message):
  # Asserts that a value equals another value.
  if unittestResults == None:
    raise Exception("Orphaned assert equals: " + message)
  if actual == expected:
    unittestResults.append((True, "OK", message))
  else:
    unittestResults.append((False, "Expected: %s\nActual: %s" % (expected, actual), message))

def fail(message):
  # Always assert an error.
  if unittestResults == None:
    raise Exception("Orphaned assert equals: " + message)
  unittestResults.append((False, "Fail.", message))

def test_if():
  global unittestResults, ok
  if False:
    fail('if false')
  ok = False
  if True:
    ok = True
  assertEquals(ok, True, 'if true')
  ok = False
  if False:
    fail('if/else false')
  else:
    ok = True
  assertEquals(ok, True, 'if/else false')
  ok = False
  if True:
    ok = True
  else:
    fail('if/else true')
  assertEquals(ok, True, 'if/else true')
  ok = False
  if False:
    fail('elseif 1')
  elif True:
    ok = True
  elif True:
    fail('elseif 2')
  else:
    fail('elseif 3')
  assertEquals(ok, True, 'elseif 4')

def test_equalities():
  global unittestResults
  assertEquals(2 == 2, True, 'Equal yes')
  assertEquals(3 == 4, False, 'Equal no')
  assertEquals(5 != 6, True, 'Not equal yes')
  assertEquals(3 == 4, False, 'Not equal no')
  assertEquals(5 < 6, True, 'Smaller yes')
  assertEquals(7 < 7, False, 'Smaller no')
  assertEquals(9 > 8, True, 'Greater yes')
  assertEquals(10 > 10, False, 'Greater no')
  assertEquals(11 <= 11, True, 'Smaller-equal yes')
  assertEquals(13 <= 12, False, 'Smaller-equal no')
  assertEquals(14 >= 14, True, 'Greater-equal yes')
  assertEquals(15 >= 16, False, 'Greater-equal no')

def test_or():
  global unittestResults
  assertEquals(True or True, True, 'Or true/true')
  assertEquals(False or True, True, 'Or false/true')
  assertEquals(True or False, True, 'Or true/false')
  assertEquals(False or False, False, 'Or false/false')

def test_and():
  global unittestResults
  assertEquals(True and True, True, 'And true/true')
  assertEquals(False and True, False, 'And false/true')
  assertEquals(True and False, False, 'And true/false')
  assertEquals(False and False, False, 'And false/false')

def test_ternary():
  global unittestResults
  assertEquals(42 if True else 99, 42, 'if true')
  assertEquals(42 if False else 99, 99, 'if true')


unittestResults = []
assertEquals(True, True, 'True')
assertEquals(False, False, 'False')
assertEquals(not False, True, 'Not true')
assertEquals(not True, False, 'Not false')
test_if()
test_equalities()
test_and()
test_or()
test_ternary()
report = unittest_report()
unittestResults = None
print(report)