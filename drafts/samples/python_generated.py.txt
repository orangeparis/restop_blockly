unittestResults = None
tv = None
timeout = None

def restop_report():
  # Create test report.
  report = []
  summary = []
  fails = 0
  for (success, log, message) in unittestResults:
    if success:
      summary.append(".")
    else:
      summary.append("F")
      fails += 1
      report.append("")
      report.append("FAIL: " + message)
      report.append(log)
  report.insert(0, "".join(summary))
  report.append("")
  report.append("Number of tests run: %d" % len(unittestResults))
  report.append("")
  if fails:
    report.append("FAILED (failures=%d)" % fails)
  else:
    report.append("OK")
  return "\n".join(report)

def unit_one(tv, timeout):
  power_on( tv )
  serial_synch( tv, timeout=timeout )


unittestResults = []
unit_one('tv1', 10)
report = restop_report()
unittestResults = None
print(report)
