unittestResults = None
device = None
timeout = None

def restop_report():
  # Create test report.
  report = []
  summary = []
  fails = 0
  for (success, log, message) in unittestResults:
    if success:
      summary.append(".")
    else:
      summary.append("F")
      fails += 1
      report.append("")
      report.append("FAIL: " + message)
      report.append(log)
  report.insert(0, "".join(summary))
  report.append("")
  report.append("Number of tests run: %d" % len(unittestResults))
  report.append("")
  if fails:
    report.append("FAILED (failures=%d)" % fails)
  else:
    report.append("OK")
  return "\n".join(report)

def unit(device, timeout):
  """ this is it """
  restopResults = []
  with api.open_session(members=[device]):
    sync( device, timeout=timeout )
    expect( device, pattern='tv', timeout=5, cancel_on='', regex='no' )
  restopResults = None


unittestResults = []
with api.open_runner():
  unit('tv', 10)
  unit('tv2', 10)
report = restop_report()
unittestResults = None
print(report)
