
from client_api import Api


api= Api()


def unit_one(tv,timeout=10):
    members=['tv','livebox']
    tv= 'tv'
    livebox='livebox'

    api._enter('function opens_session')
    with api.open_session(*members):

        api._enter('function expect')
        api.expect(tv,timeout=timeout)
        api._leave('function expect')

