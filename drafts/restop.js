

    // javascript
  var functionName = Blockly.JavaScript.provideFunction_(
      'unittest_report',
      [ 'function ' + Blockly.JavaScript.FUNCTION_NAME_PLACEHOLDER_ + '() {',
        '  // Create test report.',
        '  var report = [];',
        '  var summary = [];',
        '  var fails = 0;',
        '  for (var x = 0; x < ' + resultsVar + '.length; x++) {',
        '    if (' + resultsVar + '[x][0]) {',
        '      summary.push(".");',
        '    } else {',
        '      summary.push("F");',
        '      fails++;',
        '      report.push("");',
        '      report.push("FAIL: " + ' + resultsVar + '[x][2]);',
        '      report.push(' + resultsVar + '[x][1]);',
        '    }',
        '  }',
        '  report.unshift(summary.join(""));',
        '  report.push("");',
        '  report.push("Number of tests run: " + ' + resultsVar +
              '.length);',
        '  report.push("");',
        '  if (fails) {',
        '    report.push("FAILED (failures=" + fails + ")");',
        '  } else {',
        '    report.push("OK");',
        '  }',
        '  return report.join("\\n");',
        '}']);

    // python
   var functionName = Blockly.Python.provideFunction_(
      'unittest_report',
      ['def ' + Blockly.Python.FUNCTION_NAME_PLACEHOLDER_ + '():',
       '  # Create test report.',
       '  report = []',
       '  summary = []',
       '  fails = 0',
       '  for (success, log, message) in ' + resultsVar + ':',
       '    if success:',
       '      summary.append(".")',
       '    else:',
       '      summary.append("F")',
       '      fails += 1',
       '      report.append("")',
       '      report.append("FAIL: " + message)',
       '      report.append(log)',
       '  report.insert(0, "".join(summary))',
       '  report.append("")',
       '  report.append("Number of tests run: %d" % len(' + resultsVar + '))',
       '  report.append("")',
       '  if fails:',
       '    report.append("FAILED (failures=%d)" % fails)',
       '  else:',
       '    report.append("OK")',
       '  return "\\n".join(report)']);

