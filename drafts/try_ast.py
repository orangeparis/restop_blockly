import ast
import codegen
#from ast import *

sample='''\

def unit(members='tv,livebox',timeout=10):
    return

open_session('livebox')
while 1:
    send("livebox",cmd='echo "hello"')
    if True:
        print("this is true")
    else:
        print('do nothing')
    break
send("livebox",cmd="i am in main section")

'''



special_functions=('power_on','send')


class MyNodeTransformer(ast.NodeTransformer):
    """

        Transform the ast code

    """

class ContextWhile(MyNodeTransformer):
    """

    """
    def visit_While(self, node):
        """
            add ctx.enter() before the while and ctx.leave() after
        :param node:
        :return:
        """
        newnode= []
        newnode.append(ctx_enter)

        self.generic_visit(node)
        newnode.append(node)

        newnode.append(ctx_leave)

        #ast.copy_location(newnode, node)
        #ast.fix_missing_locations(newnode)
        return newnode

    def visit_If(self, node):
        """
            add ctx.enter() before the if and ctx.leave() after
        :param node:
        :return:
        """
        newnode= []
        newnode.append(ctx_enter)

        newnode.append(node)
        self.generic_visit(node)

        newnode.append(ctx_leave)

        #ast.copy_location(newnode, node)
        #ast.fix_missing_locations(newnode)
        return newnode


    def visit_FunctionDef(self,node):
        """
        FunctionDef(self,name, args, body, decorator_list, returns):
            :param name:
            :param args:
            :param body:
            :param decorator_list:
            :param returns:
            :return:
        """
        #name= node.value.func.id
        return node

    def visit_Call(self,node):
        """

        :param node:
        :return:
        """
        name = node.func.id
        if name in special_functions:
            # modify it
            source = codegen.to_source(node)
            source = 'api.%s' % source
            node= ast.parse(source).body[0]
            node=node.value

        return node





tree = ast.parse(sample)

# ctx_enter= ast.parse(ctx_enter).body[0]
# ctx_leave= ast.parse(ctx_leave).body[0]


ctx_enter= ast.parse('api._enter()').body[0]
ctx_leave= ast.parse('api._leave()').body[0]

call_function= ast.parse('open_session(first,members=[])').body[0]
source= codegen.to_source(call_function)

source= 'apii.%s' % source
modified_method= ast.parse(source).body[0]


call_method= ast.parse('apii.open_session(first,members=[])').body[0]

print codegen.to_source(call_method)


# apply code modifications
tree = ContextWhile().visit(tree)

# Add lineno & col_offset to the nodes we created
#ast.fix_missing_locations(tree)
#exec(compile(tree, filename="<ast>", mode="exec"))
print "===================================="
modified_source= codegen.to_source(tree)
print modified_source
print "===================================="


class Context(object):

    def enter(self):
        print('enter')
    def leave(self):
        print('leave')

ctx= Context()

def open_session(members):
    print("open_session")

def send(alias,cmd):
    print("send")


code= compile(tree, filename="<ast>", mode="exec")

exec(code)




print "Done"


