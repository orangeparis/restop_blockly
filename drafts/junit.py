pattern='''\
<?xml version="1.0" encoding="UTF-8"?>
<testsuites disabled="" errors="" failures="" name="" tests="" time="">
    <testsuite disabled="" errors="" failures="" hostname="" id=""
        name="" package="" skipped="" tests="" time="" timestamp="">
        <properties>
            <property name="" value=""/>
            <property name="" value=""/>
        </properties>
        <testcase assertions="" classname="" name="" status="" time="">
            <skipped/>
            <error message="" type=""/>
            <error message="" type=""/>
            <failure message="" type=""/>
            <failure message="" type=""/>
            <system-out/>
            <system-out/>
            <system-err/>
            <system-err/>
        </testcase>
        <testcase assertions="" classname="" name="" status="" time="">
            <skipped/>
            <error message="" type=""/>
            <error message="" type=""/>
            <failure message="" type=""/>
            <failure message="" type=""/>
            <system-out/>
            <system-out/>
            <system-err/>
            <system-err/>
        </testcase>
        <system-out/>
        <system-err/>
    </testsuite>
    <testsuite disabled="" errors="" failures="" hostname="" id=""
        name="" package="" skipped="" tests="" time="" timestamp="">
        <properties>
            <property name="" value=""/>
            <property name="" value=""/>
        </properties>
        <testcase assertions="" classname="" name="" status="" time="">
            <skipped/>
            <error message="" type=""/>
            <error message="" type=""/>
            <failure message="" type=""/>
            <failure message="" type=""/>
            <system-out/>
            <system-out/>
            <system-err/>
            <system-err/>
        </testcase>
        <testcase assertions="" classname="" name="" status="" time="">
            <skipped/>
            <error message="" type=""/>
            <error message="" type=""/>
            <failure message="" type=""/>
            <failure message="" type=""/>
            <system-out/>
            <system-out/>
            <system-err/>
            <system-err/>
        </testcase>
        <system-out/>
        <system-err/>
    </testsuite>
</testsuites>
'''

sample= '''\
<?xml version="1.0" encoding="UTF-8"?>
<testsuite>

  <!-- if your classname does not include a dot, the package defaults to "(root)" -->
  <testcase name="my testcase" classname="my package.my classname" time="29">

    <!-- If the test didn't pass, specify ONE of the following 3 cases -->

    <!-- option 1 --> <skipped />
    <!-- option 2 --> <failure message="my failure message">my stack trace</failure>
    <!-- option 3 --> <error message="my error message">my crash report</error>

    <system-out>my STDOUT dump</system-out>

    <system-err>my STDERR dump</system-err>

  </testcase>

</testsuite>

'''