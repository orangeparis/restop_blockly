
import inspect



def myfunc():
    """

    :return:
    """
    return True



def myone_arg_func(alias):
    """

    :param alias:
    :return:
    """
    return alias

def myfunc_with_kwargs(alias,timeout=8, regex="hello", **kwargs):

    return alias



def magical_way(f):

    spec= inspect.getargspec(f)

    sig= inspect.formatargspec(spec)



    r= inspect.getargspec(f)[0]
    return r



for f in [myfunc,myone_arg_func,myfunc_with_kwargs]:

    rc= magical_way(f)
    continue

print "Done"