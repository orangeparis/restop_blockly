
_api=None


def get_api():
    """

    :return:
    """
    global _api
    if not _api:
        _api= Api()
    return _api


class Api(object):
    """


    """
    def open_session(self,members):
        """

        :param members:
        :return:
        """
        return

    def close_session(self):
        """

        :return:
        """
        return


class Session:
    """


    """
    def __init__(self,members):
        """

        :param members:
        """
        self.api = get_api()
        self.members= members

    def __enter__(self):
        """

        :return:
        """
        print "enter session"
        self.session = self.api.open_session(members)

    def __exit__(self, type, value, traceback):
        """

        :param type:
        :param value:
        :param traceback:
        :return:
        """
        print "close session"
        self.api.close_session()


# code

members=['tv']
with Session(members) as session:

    print "first"
    #raise RuntimeError()
    print "second"

