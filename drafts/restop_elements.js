Blockly.Blocks['restop_expect'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("expect");
    this.appendValueInput("members")
        .setCheck("String")
        .appendField(new Blockly.FieldDropdown([["tv", "tv"], ["livebox", "livebox"], ["hub", "hub"]]), "NAME");
    this.appendValueInput("pattern")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("pattern");
    this.setInputsInline(false);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

//javascript
Blockly.JavaScript['restop_expect'] = function(block) {
  var dropdown_name = block.getFieldValue('NAME');
  var value_members = Blockly.JavaScript.valueToCode(block, 'members', Blockly.JavaScript.ORDER_ATOMIC);
  var value_pattern = Blockly.JavaScript.valueToCode(block, 'pattern', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = '...';
  return code;
};


//python
Blockly.Python['restop_expect'] = function(block) {
  var dropdown_name = block.getFieldValue('NAME');
  var value_members = Blockly.Python.valueToCode(block, 'members', Blockly.Python.ORDER_ATOMIC);
  var value_pattern = Blockly.Python.valueToCode(block, 'pattern', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '...';
  return code;
};