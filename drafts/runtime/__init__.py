

class Runtime:
    """



    """
    def __init__(self,name="main"):
        """


        :param name:
        """
        self.name=name
        self.indent=0
        self.indent_step= 2

    def __enter__(self):
        """



        :return:
        """
        print "__enter__"
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """

        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        print ('__exit__')



    def out(self,message):
        """

        :param message:
        :return:
        """
        print "%s%s" % (" " * self.indent,message)

    def enter(self,section="main"):
        """

        :param section:
        :return:
        """
        self.out("enter section: %s" % section)
        self.indent += self.indent_step

    def leave(self):
        """

        :param section:
        :return:
        """
        self.out ('leave section')
        self.indent -= self.indent_step


with Runtime() as ctx:


    ctx.out("open session")
    ctx.enter("while_block")
    while 1:
        ctx.out("I am in while section")
        break
    ctx.leave()
    ctx.out("I am in main section")





