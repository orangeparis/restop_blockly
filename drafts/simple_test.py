unittestResults = None
members = None
item = None

def unittest_report():
  # Create test report.
  report = []
  summary = []
  fails = 0
  for (success, log, message) in unittestResults:
    if success:
      summary.append(".")
    else:
      summary.append("F")
      fails += 1
      report.append("")
      report.append("FAIL: " + message)
      report.append(log)
  report.insert(0, "".join(summary))
  report.append("")
  report.append("Number of tests run: %d" % len(unittestResults))
  report.append("")
  if fails:
    report.append("FAILED (failures=%d)" % fails)
  else:
    report.append("OK")
  return "\n".join(report)

def assertEquals(actual, expected, message):
  # Asserts that a value equals another value.
  if unittestResults == None:
    raise Exception("Orphaned assert equals: " + message)
  if actual == expected:
    unittestResults.append((True, "OK", message))
  else:
    unittestResults.append((False, "Expected: %s\nActual: %s" % (expected, actual), message))

def do_something():
  global members, unittestResults
  members = 'tv,livebox'.split(',')
  unittestResults = True

def start():
  global item
  item = open_session()

def open_session():
  global members
  members = ['tv', 'livebox']
  return 'OK'


unittestResults = []
assertEquals(open_session(), 'OK', 'first')
report = unittest_report()
unittestResults = None
print(report)
